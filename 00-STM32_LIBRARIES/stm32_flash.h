/**
 *
 *  \file       flash.h
 *  \public
 *  \defgroup   FLASH
 *  \date       10/22/2014
 *
 *  \section LICENSE
 * This file is Copyright NSC LLC 2014 and is a confidental 
 * and proprietary work.  This file is not for redistribution 
 * unless otherwise specified by license agreement. 
 *
 * \brief   Defines and prototypes
 *  
 * \addtogroup     FLASH
 *
 * @{
 */
#ifndef FLASH_H
#define FLASH_H
#ifdef __cplusplus
 extern "C" {
#endif
   
/* Includes -----------------------------------------------------------------*/
//#include "main.h"
#include "defines.h"
#include "stm32f0xx.h"
   
/* Public typedef -----------------------------------------------------------*/

/* Public define ------------------------------------------------------------*/
//#define FLASH_PAGE_SIZE         ((uint32_t)0x00000400)   /* FLASH Page Size */  // REMOVED conficet with hal_flash_ex

#define FLASH_BASE_ADDRESS      ((uint32_t)0x08000000)   /* Start of Flash */

#define FLASH_END_ADDR          ((uint32_t)0x08020000)   /* End @ of user Flash area */

#define RAM_BUFFER_FOR_PAGE     FLASH_PAGE_SIZE/4

#define WORD_SIZE               4
#define DATA_32                 ((uint32_t)0x12345678)

#define LOCAL_IMAGE              0x00
#define STORED_IMAGE             0x01
   
/* Public macro -------------------------------------------------------------*/


/* Public variables ---------------------------------------------------------*/


/* Public function prototypes -----------------------------------------------*/
void FLASH_write_entry(uint32_t start_address, uint32_t stop_address, uint8_t *data);
void FLASH_read_entry(uint32_t Start_Addr, uint32_t End_Addr, uint8_t *data);
void FLASH_copy_page(uint32_t start_address, uint32_t *data);
void FLASH_write_page(uint32_t address, uint32_t *data);
void FLASH_erase_page(uint32_t address);
HAL_StatusTypeDef FLASH_update_page(uint32_t start_address, uint32_t stop_address, uint32_t *data);
uint8_t FLASH_image_verify(uint8_t image);

#ifdef __cplusplus
}
#endif
#endif
/// @}
