/**
 *
 *  \file       flash.c
 *  \public
 *  \defgroup   FLASH
 *  \date       10/22/2014
 *
 *  \section LICENSE
 * This file is Copyright NSC LLC 2014 and is a confidental 
 * and proprietary work.  This file is not for redistribution 
 * unless otherwise specified by license agreement. 
 *
 * \brief   Flash memory utilities 
 *  
 * \addtogroup     FLASH
 *
 * @{
 */
/* Includes ------------------------------------------------------------------*/
#include <string.h>
//#include "common.h"
#include "stm32_flash.h"

#define STORED_IMAGE_CRC_ADDR     0 
#define CURRENT_CRC_ADDR          0
#define APP_START_ADDR            0
#define APP_END_ADDR              0
#define STORED_IMAGE_FW_VER_ADDR  0
#define CURRENT_FW_VERSION_ADDR   0
#define STORED_IMAGE_START_ADDR   0

/* Private typedef -----------------------------------------------------------*/
typedef enum
{
    FAILED = 0, PASSED = !FAILED
} TestStatus;
/* Private define ------------------------------------------------------------*/
#define FLASH_COMPLETE   1

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
//__IO FLASH_Status FLASHStatus = FLASH_COMPLETE;
__IO TestStatus MemoryProgramStatus = PASSED;

typedef union
{
    uint32_t word[RAM_BUFFER_FOR_PAGE];
    uint8_t byte[RAM_BUFFER_FOR_PAGE*WORD_SIZE];
}page_data_union;

page_data_union page;

CRC_HandleTypeDef   CrcHandle;

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
uint32_t get_start_of_page(uint32_t address);
uint32_t get_next_page_boundary(uint32_t address);

static int InitializeCRC(void)
{
    /*##-1- Configure the CRC peripheral #######################################*/
  CrcHandle.Instance = CRC;

  /* The default polynomial is used */
  CrcHandle.Init.DefaultPolynomialUse    = DEFAULT_POLYNOMIAL_ENABLE;

  /* The default init value is used */
  CrcHandle.Init.DefaultInitValueUse     = DEFAULT_INIT_VALUE_ENABLE;

  /* The input data are not inverted */
  CrcHandle.Init.InputDataInversionMode  = CRC_INPUTDATA_INVERSION_NONE;

  /* The output data are not inverted */
  CrcHandle.Init.OutputDataInversionMode = CRC_OUTPUTDATA_INVERSION_DISABLE;

  /* The input data are 32 bits lenght */
  CrcHandle.InputDataFormat              = CRC_INPUTDATA_FORMAT_WORDS;

  if (HAL_CRC_Init(&CrcHandle) != HAL_OK)
  {
    /* Initialization Error */
    return 1;
  }
  return 0;

}

/**
 *
 * Description : Flash write
 *
 * Arguments   : uint32_t start_address, uint32_t stop_address, 
 *               uint8_t *data 
 * 
 * Returns     : void
 *
 */
void FLASH_write_entry(uint32_t start_address, uint32_t stop_address, uint8_t *data)
{
    uint32_t PageCounter = 0x00;
    uint32_t NbrOfPage = 0x00;
    uint32_t i=0, loop=0;
    uint32_t start_page, stop_page, current_block;
    uint16_t byte_data_offset;
    uint32_t length, LenCounter = 0x00;


    if(stop_address > FLASH_END_ADDR)            //Requested STOP Address exceeds FLASH SPACE
        return;

    start_page = get_start_of_page(start_address);
    stop_page = get_next_page_boundary(stop_address);
    /* Define the number of page to be written too */
    NbrOfPage = ((stop_page - start_page) / FLASH_PAGE_SIZE);
    current_block = start_page;

    byte_data_offset = (start_address % FLASH_PAGE_SIZE);
    length = (stop_address - start_address);


    /* Erase the FLASH pages */
    for(PageCounter = 0; (PageCounter < NbrOfPage); PageCounter++)
    {
        FLASH_copy_page(current_block, page.word);

        if(((length - LenCounter)+byte_data_offset) > (FLASH_PAGE_SIZE))
        {
            loop = (FLASH_PAGE_SIZE) - byte_data_offset;
        }
        else
        {
            loop = length-LenCounter;
        }
        for(i=0; i<loop; i++)
        {
            page.byte[byte_data_offset+i] = data[LenCounter];
            LenCounter++;
        }

        FLASH_erase_page(current_block);

        // Program a block of Flash memory
        FLASH_update_page(current_block, (current_block+FLASH_PAGE_SIZE), page.word);

        current_block += FLASH_PAGE_SIZE;
    }
}

/**
 *
 * Description : Flash read
 *
 * Arguments   : uint32_t Start_Addr, uint32_t End_Addr, 
 *               uint8_t data 
 * 
 * Returns     : void
 *
 */
void FLASH_read_entry(uint32_t Start_Addr, uint32_t End_Addr, uint8_t *data)
{
    uint16_t i;
    uint32_t Address;

    Address = Start_Addr;
    i = 0;
    if(!(Start_Addr%WORD_SIZE) && !(End_Addr%WORD_SIZE))
    {
        while(Address < End_Addr)
        {
            page.word[i] = *(__IO uint32_t *)Address;
            Address = Address + WORD_SIZE;
            i++;
        }
    }
    else
    {
        while(Address < End_Addr)
        {
            page.byte[i] = *(__IO uint8_t *)Address;
            Address = Address + 1;
            i++;
        }
    }
    i = End_Addr - Start_Addr;
    memcpy(data, page.byte, i);
}

/**
 *
 * Description : Flash copy page
 *
 * Arguments   : uint32_t start_address, uint32_t *data
 * 
 * Returns     : void
 *
 */
void FLASH_copy_page(uint32_t start_address, uint32_t *data)
{
    uint16_t i;
    uint32_t Address, End_Addr;
    HAL_FLASH_Unlock();

    Address = get_start_of_page(start_address);
    End_Addr = Address + FLASH_PAGE_SIZE;

    __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPERR); 
    i = 0;
    while(Address < End_Addr)
    {
        data[i] = *(__IO uint32_t *)Address;
        Address = Address + WORD_SIZE;
        i++;
    }

    HAL_FLASH_Lock();

}

/**
 *
 * Description : Write page to flash
 *
 * Arguments   : uint32_t address, uint32_t *data 
 * 
 * Returns     : void
 *
 */
void FLASH_write_page(uint32_t address, uint32_t *data)
{
  uint32_t start_address, end_addr;
  
  FLASH_erase_page(address);

  start_address = get_start_of_page(address);
  end_addr = start_address + FLASH_PAGE_SIZE;
  
  // Program a block of Flash memory  
  FLASH_update_page(start_address, end_addr, data);
}


/**
 *
 * Description : Flash erase page
 *
 * Arguments   : uint32_t address
 * 
 * Returns     : void
 *
 */
void FLASH_erase_page(uint32_t address)
{
    FLASH_EraseInitTypeDef erase;
    uint32_t pageError = 0;

    HAL_FLASH_Unlock();

    /* Erase the user Flash area
      (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/

    /* Clear pending flags (if any) */  
    __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPERR); 
    
    erase.TypeErase = FLASH_TYPEERASE_PAGES;
    erase.PageAddress = get_start_of_page(address);
    erase.NbPages = 1;
  
    if(HAL_FLASHEx_Erase(&erase, &pageError) != HAL_OK)
    {
        /* Error occurred while sector erase. 
            User can add here some code to deal with this error  */
        while(1)
        {
        }
    }

    HAL_FLASH_Lock();
}

/**
 *
 * Description : Flash update page.
 *               Program a block of Flash memory, 4 bytes at a time.
 *               Note: The stop_address is one byte past the actual end of data address.
 *
 * Arguments   : uint32_t start_address, uint32_t stop_address, 
 *               uint32_t *data 
 * 
 * Returns     : void
 *
 */
#ifdef DEBUGOPTIMIZE
#pragma optimize=none  // SMC TEST
#endif

HAL_StatusTypeDef FLASH_update_page(uint32_t start_address, uint32_t stop_address, uint32_t *data)
{
    uint16_t i;
    uint32_t Address;
    HAL_StatusTypeDef status = HAL_OK;
    
    
    HAL_FLASH_Unlock();
    
    /* Clear pending flags (if any) */ 
    __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPERR); 
    
    i=0;
    Address = start_address;
    
    while(Address < stop_address)
    {
        // Did the programming of the 32-bit word complete successfully?
        if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, Address, data[i]) == HAL_OK)  //FLASH_COMPLETE == 1 which would cause test to always fail )
        {
            // Yes - Step to the next word
            Address = Address + WORD_SIZE;
            i++;
        }
        else
        {
            // No - Error occurred while writing data in Flash memory (ie. timeout while waiting for last operation to complete!).
            status = HAL_ERROR;
            
            // HOWEVER, Still continue and attempt to program rest of Flash memory
            Address = Address + WORD_SIZE;
            i++;
        }
    }
    
    HAL_FLASH_Lock();

    // Return status (HAL_OK or HAL_ERROR if we detected a problem along the way...)
    return (status);
}

/**
 *
 * Description : Get start of page
 *
 * Arguments   : uint32_t address
 * 
 * Returns     : address
 *
 */
uint32_t get_start_of_page(uint32_t address)
{
    uint32_t modulo;
    modulo = address % FLASH_PAGE_SIZE;

    if(modulo)
    {
        address -= modulo;
    }
    return address;
}

/**
 *
 * Description : Get next start of page
 *
 * Arguments   : uint32_t address
 * 
 * Returns     : return page boundary
 *
 */
uint32_t get_next_page_boundary(uint32_t address)
{
    uint32_t modulo;
    modulo = address % FLASH_PAGE_SIZE;

    if(modulo)
    {
        address += FLASH_PAGE_SIZE - modulo;
    }
    return address;
}

/**
 *
 * Description : Verify image in flash
 *
 * Arguments   : uint8_t image 
 * 
 * Returns     : 0 = pass or 1 = fail
 *
 */ 
#ifdef DEBUGOPTIMIZE
#pragma optimize=none
#endif

uint8_t FLASH_image_verify(uint8_t image)
{
   uint8_t  status = 1;
   uint8_t  temp[2];
   uint32_t  stored_FW_Ver, expected_crc;
   uint32_t  addr, crc_address;
   uint32_t  crc_value = 0;
   uint32_t  crc_len;
   
   
   // Are we verifying the local image?
   if (image != LOCAL_IMAGE)
   {
       // No - Get the update stored image firmware version address
       addr = STORED_IMAGE_FW_VER_ADDR;
   }
   else
   {
       // Yes - Get the current image firmware version address
       addr = CURRENT_FW_VERSION_ADDR;
   }

   // Read image firmware version address from last location in image
   FLASH_read_entry(addr, (addr+2), temp);
   stored_FW_Ver = ((uint16_t)temp[1]<<8) + temp[0];
   
   // If invalid firmware version number then error return
   if(!stored_FW_Ver)
       return (1);
   
   // Initialize the CRC options for using the CRC register (error return if fails)
   if (InitializeCRC())
      return (1);
   
   // Are we verifying the local image?
   if (image != LOCAL_IMAGE)
   {
       // No - Configure for update stored image CRC test 
       crc_address = STORED_IMAGE_CRC_ADDR;
       addr = STORED_IMAGE_START_ADDR;
   }
   else
   {
       // Yes - Configure for current image CRC test        
       crc_address = CURRENT_CRC_ADDR;
       addr = APP_START_ADDR;
   }
  
   expected_crc = *(__IO uint32_t *)crc_address;
   
   // Compute the CRC
   crc_len = (APP_END_ADDR - APP_START_ADDR) / 4 + 1;
   crc_value = HAL_CRC_Calculate(&CrcHandle, (uint32_t *)addr, crc_len);   

   // Did the image verify
   if(crc_value == expected_crc)
     status = 0;                    // Yes - We have a valid image
   else
     status = 1;                    // No  - We have an invalid image
   
   HAL_CRC_DeInit(&CrcHandle);
   return status;

}


/// @}
