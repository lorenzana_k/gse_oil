//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
/*!
  \file   SystemEvents.h
  \author Carlos Luis Bernal
  \date   Dec 11, 2010
  \brief  
*/
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
#ifndef __SYTEM_EVENTS_H__
#define __SYTEM_EVENTS_H__

void EventTickHanlder(void);

#define GENERATED_EVENT_CODE
  extern EventControl BlinkEventControl;              \
  extern void BlinkEventFunction(void);               \

  extern EventControl ESP_stackControl;               \
  extern void ESP_stackFunction(void);                \

  extern EventControl ESP_InitEventControl;           \
  extern void ESP_InitEventFunction(void);            \

  extern EventControl CAP_DataRdyEventControl;        \
  extern void CAP_DataRdyEventFunction(void);         \

#define GENERATED_EVENTS                                          \
  { &BlinkEventControl, BlinkEventFunction },                     \
  { &ESP_stackControl, ESP_stackFunction },                       \
  { &ESP_InitEventControl, ESP_InitEventFunction },                       \
  { &CAP_DataRdyEventControl, CAP_DataRdyEventFunction },         \
  
#define EMBER_AF_GENERATED_EVENT_STRINGS      \
  "Blink Event",                              \
  "ESP stack Event",                          \
  "ESP Initiator Event",                      \
  "Capacitive converter Event",               \

#endif
