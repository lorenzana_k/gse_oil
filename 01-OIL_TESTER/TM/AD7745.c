/** 
 * @author  
 * @email   
 * @website 
 * @link    
 * @version 
 * @ide     
 * @license 
 * @brief   
 *
\verbatim
   ----------------------------------------------------------------------
    Copyright (c) 2016 

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without restriction,
    including without limitation the rights to use, copy, modify, merge,
    publish, distribute, sublicense, and/or sell copies of the Software, 
    and to permit persons to whom the Software is furnished to do so, 
    subject to the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
    AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
   ----------------------------------------------------------------------
\endverbatim
 */
#include "AD7745.h"
#include "stm32f070xb.h"
#include "string.h"
#include "EventsEngine.h"
#include "SystemEvents.h"

#define I2C_ADDRESS                 0x90
#define AD7745_RESET_COMMAND_ADD    0xBF

#define POW_2_16        (float)65536.0  //2^16
#define C_REF_CONST     (float)4.096
#define C_RADIO_CAP_DAC (float)3.2
#define C_REF           (double)4.096 
#define CONST           (double)8388608.0 // x^24

#define GETS_PICO_F(x)     (double)((((double)x-CONST)/CONST)*C_REF)
#define GETS_REAL_CAP(x)     (double)( GETS_PICO_F(x) * (float)(-3.5) )
  
EventControl CAP_DataRdyEventControl;        
void CAP_DataRdyEventFunction(void);   
 
static uint8_t data[19];
static AD7745_Samples samples;

/**
* @brief      Initializes the capacitance to digital converter module.
*
* @return     { description_of_the_return_value }
*/
AD7745_Result_t AD7745_Init(void)
{
  /* I2C status code*/
  TM_I2C_Result_t i2cRes; 
  AD7745_Registers *reg;
  GPIO_InitTypeDef   GPIO_InitStructure;
  int i;

  /* Initialize I2C module */
  if ((i2cRes = TM_I2C_Init(I2C1,TM_I2C_PinsPack_1,48000000)) == TM_I2C_Result_Ok){
      debug_println("I2C module init successfully!");
  } else {
      debug_println("I2C init error. Status: %d", i2cRes);
      return AD7745_Result_Error;
  }

  /* Reset Chip */
  AD7745_Reset();

  if ((i2cRes = TM_I2C_ReadMulti(I2C1,I2C_ADDRESS, STATUS,(uint8_t*)&data, AD7745_REGISTERS_AMT)) == TM_I2C_Result_Ok){
      for(i=STATUS; i<=VOLT_GAIN_L;i++)
      {
      debug_println("Address 0x%X = %d",i,data[i]);      
      }
  } else {
      debug_println("I2C read error. Status: %d", i2cRes);
      return AD7745_Result_Error;
  }

  reg = (AD7745_Registers*)&data;

  /**
   * configurates the capacitive to digital converter as follows:
   * Enable Channel 1
   * Differential mode
   * Continuous mode
   */
  reg->Cap_Setup  = (CAPSET_CAPEN | CAPSET_CAPDIFF);  // Enables capacitive channel 1 for conversion, sets differential mode
  reg->VT_Setup = VTSET_VTCHOP;
  reg->EXC_Setup = EXCSET_EXCB | EXCSET_EXCA | EXCSET_EXCLVL1 | EXCSET_EXCLVL0;
  reg->Configuration = CONFIG_VTFS1 | CONFIG_CAPFS2 | CONFIG_CAPFS1 | CONFIG_CAPFS0 ;
  reg->Cap_DAC_A = DAC_A_DACAENA | (Cap_DAC.CapDAC_A&DAC_A_DACA);
  reg->Cap_DAC_B = DAC_B_DACBENB | (Cap_DAC.CapDAC_B&DAC_B_DACB);

  if ((i2cRes = TM_I2C_WriteMulti(I2C1,I2C_ADDRESS, STATUS ,(uint8_t*)&data, AD7745_REGISTERS_AMT))== TM_I2C_Result_Ok){
      debug_println("AD7745 Initialize successfully!");
  } else {
      debug_println("AD7745 Initialize Error!");
  }

  if ((i2cRes = TM_I2C_ReadMulti(I2C1,I2C_ADDRESS, 0x00,(void*)&data, AD7745_REGISTERS_AMT)) == TM_I2C_Result_Ok){
      for(i=STATUS; i<=VOLT_GAIN_L;i++)
      {
      debug_println("Address 0x%X = %d",i,data[i]);      
      }
  } else {
      debug_println("I2C read error. Status: %d", i2cRes);
      return AD7745_Result_Error;
  }

  /**
   * Enable External interruption on RDY line, it is used to collect all converted data and store it.
   */
/* Enable GPIOC clock */
__HAL_RCC_GPIOB_CLK_ENABLE();

/* Configure PB.5 pin as input floating */
GPIO_InitStructure.Mode = GPIO_MODE_IT_FALLING;
GPIO_InitStructure.Pull = GPIO_NOPULL;
GPIO_InitStructure.Pin = GPIO_PIN_5;
HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

/* Enable and set EXTI line 4_15 Interrupt to the lowest priority */
HAL_NVIC_SetPriority(EXTI4_15_IRQn, 2, 0);
HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);

  return AD7745_Result_Ok;
}

AD7745_Result_t AD7745_Reset(void)
{
  /* I2C status code*/
  TM_I2C_Result_t i2cRes; 

  if ((i2cRes = TM_I2C_WriteNoRegister(I2C1,I2C_ADDRESS,AD7745_RESET_COMMAND_ADD)) == TM_I2C_Result_Ok)
  {
      debug_println("AD7745 chip has been reset" );
  } else {
      debug_println("I2C Reset error. Status: %d",i2cRes);
  }

  return i2cRes == TM_I2C_Result_Ok? AD7745_Result_Ok: AD7745_Result_Error;
}
 
AD7745_Result_t AD7745_Read_Data(uint32_t* CapData)
{
    AD7745_Registers *reg = (AD7745_Registers*)&data;
    TM_I2C_Result_t i2cRes;
    uint32_t CapDataInput; 
    (void)i2cRes;
    if ((i2cRes = TM_I2C_ReadMulti(I2C1,I2C_ADDRESS, STATUS,(void*)&data, 4)) == TM_I2C_Result_Ok){
        CapDataInput = (uint32_t)((reg->Cap_Data_H)<<16) | ((reg->Cap_Data_M)<<8) | (reg->Cap_Data_L);
        debug_println("Status = 0x%02X Cap Data = 0x%08X, Capacitance = %f, RealCap = %f ",reg->Status,CapDataInput,GETS_PICO_F(CapDataInput),GETS_REAL_CAP(CapDataInput));
    } else {
        debug_println("I2C read error. Status: %d", i2cRes);
        return AD7745_Result_Error;
    }

    memcpy((void*)CapData,(void*)&CapDataInput,sizeof(CapDataInput));
    return AD7745_Result_Ok;
}

AD7745_Result_t AD7745_Take_Samples(void)
{
  TM_I2C_Result_t i2cRes;
  AD7745_Registers *reg;

  reg = (AD7745_Registers*)&data;
  /* Config continoues mode */
  reg->Cap_Setup  = (CAPSET_CAPEN | CAPSET_CAPDIFF | CAPSET_CAPCHOP);  // Enables capacitive channel 1 for conversion, sets differential mode
  reg->EXC_Setup = (EXCSET_EXCB | EXCSET_EXCA_NEG | EXCSET_EXCLVL1 | EXCSET_EXCLVL0);
  reg->Configuration = CONT_CONVERSION_MODE ;
  reg->Cap_DAC_A =  DAC_A_DACAENA | (Cap_DAC.CapDAC_A&DAC_A_DACA);
  reg->Cap_DAC_B =  DAC_B_DACBENB | (Cap_DAC.CapDAC_B&DAC_B_DACB);

  if ((i2cRes = TM_I2C_WriteMulti(I2C1,I2C_ADDRESS, STATUS ,(uint8_t*)&data, AD7745_REGISTERS_AMT))== TM_I2C_Result_Ok){
      debug_println("AD7745 Continuous mode started!");
  } else {
      debug_println("AD7745 i2c Error!");
  }

  samples.PendingSamples = 100;
  samples.WrPt = (uint32_t*)&samples.SampleBuffer;

  return AD7745_Result_Ok;
}

AD7745_Result_t AD7745_ReadCalib(AD7745_cal* CapDac)
{
    TM_I2C_Result_t i2cRes; 
    AD7745_cal CapDac_Temp;
    float temp;

    (void)i2cRes;
    if ((i2cRes = TM_I2C_ReadMulti(I2C1,I2C_ADDRESS, STATUS,(void*)&data, AD7745_REGISTERS_AMT)) == TM_I2C_Result_Ok){
        CapDac_Temp.rawCapacitiveOffsetCalibration = (uint16_t)((data[CAP_OFFSET_H]<<8) | data[CAP_OFFSET_L]);
        CapDac_Temp.rawCapacitiveGainCalibration =   (uint16_t)((data[CAP_GAIN_H]<<8) | data[CAP_GAIN_L]);

        //Fgain
        temp = ((float)CapDac_Temp.rawCapacitiveGainCalibration + POW_2_16 ) / POW_2_16;
        temp = C_REF_CONST * temp;
        CapDac_Temp.C_CAPDAC = C_RADIO_CAP_DAC * temp;

        debug_println("Cap Offset = %04X  Cap Gain = %04X",CapDac_Temp.rawCapacitiveOffsetCalibration,CapDac_Temp.rawCapacitiveGainCalibration);
        debug_println("C_CapDac = %f",CapDac_Temp.C_CAPDAC);

        memcpy((void*)CapDac,(void*)&CapDac_Temp,sizeof(CapDac_Temp));
    } else {
        debug_println("I2C read error. Status: %d", i2cRes);
        return AD7745_Result_Error;
    }

    return AD7745_Result_Ok;
}

/* Reading data from cap converter*/
void CAP_DataRdyEventFunction(void)
{
    TM_I2C_Result_t i2cRes;
    AD7745_Registers *reg = (AD7745_Registers*)&data;
    EventControlSetInactive(CAP_DataRdyEventControl);
    if (AD7745_Read_Data(samples.WrPt) == AD7745_Result_Ok)
    {
      samples.CapBuffer[100-samples.PendingSamples] = GETS_REAL_CAP(*samples.WrPt);
      samples.WrPt++;
      samples.PendingSamples--;
      if (samples.PendingSamples == 0)
      {
          samples.PendingSamples = 100;
          samples.WrPt = (uint32_t*)&samples.SampleBuffer;
          #if 1
          /* Idle mode... */
          reg->Cap_Setup  = (CAPSET_CAPEN | CAPSET_CAPDIFF);  // Enables capacitive channel 1 for conversion, sets differential mode
          reg->VT_Setup = VTSET_VTCHOP;
          reg->EXC_Setup = EXCSET_EXCB_NEG | EXCSET_EXCA | EXCSET_EXCLVL1 | EXCSET_EXCLVL0;
          reg->Configuration = CONFIG_VTFS1 | CONFIG_CAPFS2 | CONFIG_CAPFS1 | CONFIG_CAPFS0 | IDLE_MODE ;
          reg->Cap_DAC_A = DAC_A_DACAENA | (Cap_DAC.CapDAC_A&DAC_A_DACA);
          reg->Cap_DAC_B = DAC_B_DACBENB | (Cap_DAC.CapDAC_B&DAC_B_DACB);

          if ((i2cRes = TM_I2C_WriteMulti(I2C1,I2C_ADDRESS, STATUS ,(uint8_t*)&data, AD7745_REGISTERS_AMT))== TM_I2C_Result_Ok){
              debug_println("AD7745 Continuous mode stopped!");
          } else {
              debug_println("AD7745 i2c Error!");
          }
          #endif
      }
    }
}
