//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
/*!
	\file   EventsEngine.c
	\author Am�rico Lorenzana Guti�rrez
	\date   Dec 13, 2010
	\brief	
*/
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Includes
//------------------------------------------------------------------------------
#include "EventsEngine.h"
#include "SystemEvents.h"
#include "defines.h"
#include <stddef.h>

GENERATED_EVENT_CODE

const char * emAfEventStrings[] = {
  "Event Tick handler",
  EMBER_AF_GENERATED_EVENT_STRINGS
};
  
EventControl EventTickControl;
void EventTickHanlder(void);

EventData emAfEvents[] = {
    {&EventTickControl ,EventTickHanlder},
    GENERATED_EVENTS
    {NULL, NULL}
};

void EventInit(void)
{   
    register EventData *events = (EventData *)&emAfEvents[0];
    register uint8_t eventIndex = 0;
    do
    {   
        events[eventIndex].control->status = EVENT_INACTIVE;
        eventIndex++;
    }while(events[eventIndex].control != NULL);
}

void RunEvents(void)
{
    register EventData *events = (EventData *)&emAfEvents[0];
    register uint8_t eventIndex = 0;


    do
    {
		if ((events[eventIndex].control)->status&EVENT_ZERO_DELAY)
		{
			events[eventIndex].handler();
		}
        eventIndex++;
    }while(events[eventIndex].control != NULL);
    
}

void EventIT(void)
{
  EventControlSetActive(EventTickControl);
}

void EventTickHanlder(void)
{
    register EventData *events = (EventData *)&emAfEvents[1];
    register uint8_t eventIndex = 0;

    EventTickControl.status = EVENT_INACTIVE;
  
    do
    {
        if ( (events[eventIndex].control)->status&EVENT_MS_TIME )
        {
            if ( !((events[eventIndex].control)->timeToExecute) )
            {
                events[eventIndex].handler();
            }
            else
            {
                (events[eventIndex].control)->timeToExecute--;
            }

        }
        eventIndex++;
    }while(events[eventIndex].control != NULL);
}

void printEvents(void)
{
  uint8_t i = 0;
  while (emAfEvents[i].control != NULL) {
    debug_print("%s  : ", emAfEventStrings[i]);
    if (emAfEvents[i].control->status == EVENT_INACTIVE) 
    {
      debug_println("inactive");
    } else 
    {
      if (emAfEvents[i].control->timeToExecute)
      {
        debug_println("%d ms", emAfEvents[i].control->timeToExecute);
      }
      else
      {
        debug_println("active");
      }
      
    }
    i++;
  }
}
