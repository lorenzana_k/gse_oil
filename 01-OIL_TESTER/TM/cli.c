#include "command-interpreter.h"
#include "AD7745.h"
#include "AD5175.h"
#include "MAX31865.h"
#include "EventsEngine.h"
#include "stm32_flash.h"


void version(void)
{
  debug_println("Oil Tester; Compiled: %s %s", __DATE__, __TIME__);
}

void capRead(void)
{
  uint32_t* temp;
  (void)AD7745_Read_Data(temp);
  (void)temp;
}

void takeSamples(void)
{
  (void)AD7745_Take_Samples();
}

void writeDAC(void)
{
  uint8_t DAC_A = (uint8_t)emberUnsignedCommandArgument(0);
  uint8_t DAC_B = (uint8_t)emberUnsignedCommandArgument(1);
  AD7745_CAP_DAC temp;
  debug_println("Cabibration data address = 0x%08X",&Cap_DAC);
  debug_println("DAC_A = 0x%02X",Cap_DAC.CapDAC_A);
  debug_println("DAC_B = 0x%02X",Cap_DAC.CapDAC_B);
  
  temp.CapDAC_A = DAC_A&DAC_A_DACA;
  temp.CapDAC_B = DAC_B&DAC_B_DACB;

  FLASH_write_entry((uint32_t)&Cap_DAC,(uint32_t)&Cap_DAC + sizeof(Cap_DAC),(uint8_t*)&temp  );

  debug_println("Cabibration data address = 0x%08X",&Cap_DAC);
  debug_println("DAC_A = 0x%02X",Cap_DAC.CapDAC_A);
  debug_println("DAC_B = 0x%02X",Cap_DAC.CapDAC_B);

}

void CalibrationPrint(void)
{
  AD7745_cal temp;
  debug_println("Cabibration data address = 0x%08X",&factory_cal);
  debug_println("Cabibration Capacitive offset 0x%04X",factory_cal.rawCapacitiveOffsetCalibration);
  debug_println("Cabibration Capacitive gain 0x%04X",factory_cal.rawCapacitiveGainCalibration);
  debug_println("Cabibration Capacitive CAPDAC %f",factory_cal.C_CAPDAC);
  debug_println("Write test");

  AD7745_ReadCalib((AD7745_cal*)&temp);
  FLASH_write_entry((uint32_t)&factory_cal,(uint32_t)&factory_cal + sizeof(factory_cal),(uint8_t*)&temp  );

  debug_println("Cabibration data address = 0x%08X",&factory_cal);
  debug_println("Cabibration Capacitive offset 0x%04X",factory_cal.rawCapacitiveOffsetCalibration);
  debug_println("Cabibration Capacitive gain 0x%04X",factory_cal.rawCapacitiveGainCalibration);
  debug_println("Cabibration Capacitive CAPDAC %f",factory_cal.C_CAPDAC);
  debug_println("Write test");
}

PGM_P PGM emberCommandTableEchoCommandArguments[] = {
  "bool indicating whether to turn echo on (1) or off (0)",
  NULL
};

void echoCommand(void)
{
  uint8_t echoOn = (uint8_t)emberUnsignedCommandArgument(0);
  if ( echoOn ) {
    emberCommandInterpreterEchoOn();
  } else {
    emberCommandInterpreterEchoOff();
  }
}

void Rheostat_WriteSimulatedTemperature(void)
{
  uint16_t value = (uint16_t)emberUnsignedCommandArgument(0);

  if(AD5175_set_RTD(value) == AD5175_Result_Ok)
  {
    debug_println("Wiper written successfully");
  }
  else
  {
    debug_println("Wiper value could not been written");
  }
}

void Rheostat_ReadSimulatedTemperature(void)
{
  float data;
  AD5175_get_RTD((float*)&data);
  debug_println("Temperature: %f",data);
}

void Rheostat_CalibrationSimulatedTemperature(void)
{
  float temp;
  debug_println("Reading calibration stored on chip");
  AD5175_ReadCalib((float*)&temp);
  debug_println("Calib %f",temp);

  debug_println("End to End resitance data address = 0x%08X",&NOMINAL_RESISTANCE);
  debug_println("End to End resitance = %f",NOMINAL_RESISTANCE);
  /* Store Tolerance*/
  FLASH_write_entry((uint32_t)&NOMINAL_RESISTANCE,(uint32_t)&NOMINAL_RESISTANCE + sizeof(NOMINAL_RESISTANCE),(uint8_t*)&temp);

  debug_println("End to End resitance data address = 0x%08X",&NOMINAL_RESISTANCE);
  debug_println("End to End resitance = %f",NOMINAL_RESISTANCE);
}
void rtdRead(void)
{
  float Temperature;

  if(MAX31865_Read_Temperature((float*)&Temperature) == MAX31865_Result_Ok)
  {
    debug_println("RTD read successfully");
  }
  else
  {
    debug_println("RTD could not been written");
  }

}

void rtdinit(void)
{
  (void) MAX31865_Init();
}



void rtdfault(void)
{
  (void)MAX31865_Fault_Detection();
}

void rtdclear(void)
{
  (void)MAX31865_Fault_clear();
}

void rtdconfig(void)
{
  uint8_t value = (uint8_t)emberUnsignedCommandArgument(0);
  MAX31865_config(value);
  debug_println("MAX31865 raw config success");
}

EmberCommandEntry CDCCommands[] = {
  emberCommandEntryActionWithDetails("read", capRead, "", "Reads data from capacitive to digital converter", NULL),
  emberCommandEntryActionWithDetails("takeSamples", takeSamples, "", "Starts taking samples", NULL),
  emberCommandEntryActionWithDetails("CalPrint", CalibrationPrint, "", "prints calibration data", NULL),
  emberCommandEntryActionWithDetails("WriteDAC", writeDAC, "uu", "Writes values of DAC_A and DAC_B", NULL),
  emberCommandEntryTerminator()
};

EmberCommandEntry rheostatCommands[] = {
  emberCommandEntryActionWithDetails("write", Rheostat_WriteSimulatedTemperature, "v", "Simulates temperature using rheostat",NULL),
  emberCommandEntryActionWithDetails("read", Rheostat_ReadSimulatedTemperature, "", "Reads simulated temperature by reading rheostat wiper resistance",NULL),
  emberCommandEntryActionWithDetails("calibration", Rheostat_CalibrationSimulatedTemperature, "", "Reads simulated temperature by reading rheostat wiper resistance",NULL),
  emberCommandEntryTerminator()
};

EmberCommandEntry rtdCommands[] = {
  emberCommandEntryActionWithDetails("init", rtdinit, "", "Initializes RTD to digital converter ", NULL),
  emberCommandEntryActionWithDetails("read", rtdRead, "", "Reads value on RTD sensor", NULL),
  emberCommandEntryActionWithDetails("fault", rtdfault, "", "Checks on error occurred on RTD sensor",NULL),
  emberCommandEntryActionWithDetails("clear", rtdclear, "", "Clears faults on RTS sensor",NULL),
  emberCommandEntryActionWithDetails("config", rtdconfig, "u", "configsn RTS sensor",NULL),
  emberCommandEntryTerminator()
};

/*
EmberCommandEntry customTableCommands[] = {
  emberCommandEntryTerminator()
};
*/

EmberCommandEntry emberCommandTable[] = {
  emberCommandEntryActionWithDetails("events", printEvents, "", "Print the list of timer events.", NULL),
  emberCommandEntryAction("version", version,"", "Args: None"),
  emberCommandEntrySubMenu("CDC", CDCCommands, ""),
  emberCommandEntrySubMenu("Rheostat", rheostatCommands, ""),
  emberCommandEntrySubMenu("rtd", rtdCommands, ""),
	emberCommandEntryActionWithDetails("echo", echoCommand, "u", "Turns echo on the command line on or off depending on the argument", emberCommandTableEchoCommandArguments),
  //emberCommandEntrySubMenu("custom", customTableCommands, ""),
  emberCommandEntryTerminator()
};
