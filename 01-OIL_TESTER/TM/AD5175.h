/**
 * @author  
 * @email   
 * @website 
 * @link    
 * @version 
 * @ide     
 * @license 
 * @brief   
 *  
\verbatim
   ----------------------------------------------------------------------
    Copyright (c) 2017

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without restriction,
    including without limitation the rights to use, copy, modify, merge,
    publish, distribute, sublicense, and/or sell copies of the Software, 
    and to permit persons to whom the Software is furnished to do so, 
    subject to the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
    AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
   ----------------------------------------------------------------------
\endverbatim
 */
#ifndef AD5175_H
#define AD5175_H

/* C++ detection */
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @addtogroup TM_STM32Fxxx_HAL_Libraries
 * @{
 */

/**
 * @defgroup tempSim
 * @brief    
 * @{
 *
 */
#include "tm_stm32_i2c.h"
#include "defines.h"

/**
 * @defgroup tempSim_Macros
 * @brief    Library defines
 * @{
 */
/**
 * @brief AD5175 Command operation
 */
#define AD5175_NOP                     0x00  /*!< NOP: do nothing. */
#define AD5175_WRITE_RDAC              0x01  /*!< Write contents of serial register data to RDAC. */
#define AD5175_READ_RDAC               0x02  /*!< Read contents of RDAC wiper register. */
#define AD5175_STORE_RDAC_TPM          0x03  /*!< Store wiper setting: store RDAC setting to 50-TP. */
#define AD5175_SW_RESET                0x04  /*!< Software reset: refresh RDAC with the last 50-TP memory stored value. */
#define AD5175_READ_TPM                 0x05  /*!< Read contents of 50-TP from the SDO output in the next frame. */
#define AD5175_READ_LAST_ADD_PROG_TPM  0x06  /*!< Read address of the last 50-TP programmed memory location. */
#define AD5175_WRITE_CR                0x07  /*!< Write contents of the serial register data to the control register. */
#define AD5175_READ_CR                 0x08  /*!< Read contents of the control register. */
#define AD5175_SW_SHUTDOWN             0x09  /*!< Software shutdown. D0 = 0; normal mode. D0 = 1; shutdown mode. */

#define MASK_DONT_CARE  0xC000
#define MASK_CMD        0x3C00
#define MASK_DATA       0x03FF

#define TOLERANCE_MASK_SIGN     (0x80)
#define TOLERANCE_MASK_INT      (0x7F)
#define TOLERANCE_MASK_FRAC     (0xFF)
#define CONST_POW_2_8           (float)(0.00390625)


#define AD5175_FORM_FRAME(CMD, DATA, DESTINATION)   \
    do{ DESTINATION = 0; DESTINATION =  (uint16_t)(((DATA&0x00FF)<<8) | ((DATA&0x0300)>>8) | ((CMD&0x000F)<<2)); }while(0)

#define RETURN_WIPER(Source,Dest) \
    do{ *Source = ((Dest&0x00FF)<<8) | ((Dest&0xFF00)>>8); }while(0)
#define AD5175_RDAC_MAX   0x03FF
#define AD5175_RDAC_MIN   0x0000
/**
 * @brief AD5175 Control Register Bit Map
 */
#define AD5175_C0 0
#define AD5175_C1 1
#define AD5175_C2 3
#define AD5175_CR_TP_ENABLE        (uint16_t)(1<<AD5175_C0)
#define AD5175_CR_RDAC_ENABLE      (uint16_t)(1<<AD5175_C1)
#define AD5175_CR_TP_FUSE_SUCCESS  (uint16_t)(1<<AD5175_C2)

 /**
 * @}
 */
 

/**
 * @brief  I2C result enumeration
 */
typedef enum {
    AD5175_Result_Ok = 0x00, /*!< Everything OK */
    AD5175_Result_Error      /*!< An error has occurred */
} AD5175_Result_t;


extern const float NOMINAL_RESISTANCE;

/**
 * @}
 */

/**
 * @defgroup 
 * @brief    Library Functions
 * @{
 */

/**
 * @brief  Initializes AD5175 module.
 */
AD5175_Result_t AD5175_Init(void);

/**
 * @brief  Sends software reset command to AD5175.
 */
AD5175_Result_t AD5175_Reset(void);

/**
 * @brief  Writes digital wiper value, Wiper full range on AD5175 is 10Kohms, with 1024-positions.
 */
AD5175_Result_t AD5175_WriteWiper(uint16_t Wiper);

/**
 * @brief  Reads digital wiper value. it goes from 0x000 to 0x03FF
 */
AD5175_Result_t AD5175_ReadWiper(uint16_t *Wiper);

AD5175_Result_t AD5175_ReadCalib(float* data);

/**
 * @brief  Writes digital wiper value that meets the conditions to simulated given temperature
 * This consist on :
 *      1) Get equivalent RTD resistance for given temperature (Rt).
 *      2) Calculate Rwiper, Rt is equivalent to parallel resistance calculated 10KOhms || Rwiper.
 *      3) Get digital code for Rwiper given by D = ((Rwiper - 35Ohms)*1024)/10KOhms
 */
AD5175_Result_t AD5175_set_RTD(float Temperature);

/**
 * @brief  Reads digital wiper stored on AD5175 and calculates the temperature that value represents.
 *          1) reads value of rheostat wiper
 *          2) Calculates resistance in parallel
 *          3) calculates Temperature with given parallel resistance
 */
AD5175_Result_t AD5175_get_RTD(float *Temperature);

/**
 * @brief  
 */
uint16_t getDigitalWiperValue(uint16_t Rw);


/**
 * @}
 */
 
/**
 * @}
 */
 
/**
 * @}
 */

/* C++ detection */
#ifdef __cplusplus
}
#endif

#endif

