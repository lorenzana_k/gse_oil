/** 
 * @author  
 * @email   
 * @website 
 * @link    
 * @version 
 * @ide     
 * @license 
 * @brief   
 *
\verbatim
   ----------------------------------------------------------------------
    Copyright (c) 2016 

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without restriction,
    including without limitation the rights to use, copy, modify, merge,
    publish, distribute, sublicense, and/or sell copies of the Software, 
    and to permit persons to whom the Software is furnished to do so, 
    subject to the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
    AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
   ----------------------------------------------------------------------
\endverbatim
 */
#include <stdlib.h>
#include <string.h>

#include "MAX31865.h"
#include "stm32f070xb.h"
#include "RTD_CAL.h"

#define SPI_CS_ACTIVE     TM_GPIO_SetPinLow(GPIOB,GPIO_Pin_12)
#define SPI_CS_INACTIVE   TM_GPIO_SetPinHigh(GPIOB,GPIO_Pin_12)

#define READ_WRITE_BIT      (1<<8)
#define SET_READ_CMD(x)     (uint8_t)(x & ~(1<<7))
#define SET_WRITE_CMD(x)    (uint8_t)(x | (1<<7))

#define REFERENCE_RESISTANCE  (float)4000.0
#define CONST_POW_2_15        (float)32768.0

#define CAL_RTD_RESISTANCE(ADC_code)  \
          (float) (((float)ADC_code*REFERENCE_RESISTANCE)/ CONST_POW_2_15)

/*
#define SPI_SEND_CMD(dataOut) \
  do{ uint8_t dummy[2]; SPI_CS_ACTIVE; TM_SPI_SendMulti(SPI2,(uint8_t*)&dataOut,(uint8_t*)&dummy,2); SPI_CS_INACTIVE; }while(0)

#define SPI_READ_REGISTER(dataOut,dataIn) \
  do{ SPI_CS_ACTIVE; TM_SPI_SendMulti(SPI2,(uint8_t*)&dataOut,(uint8_t*)&dummy,2); SPI_CS_INACTIVE; }while(0)
*/


void SPI_WRITE_REG(uint8_t reg, uint8_t data);
uint8_t SPI_READ_REG(uint8_t reg);
void SPI_READ_MULT_REGS(uint8_t reg, uint8_t *data, uint8_t Count);

/**
 * @brief      { function_description }
 *
 * @param[in]  reg   The register to read
 * @param[in]  data  The data
 */
void SPI_WRITE_REG(uint8_t reg, uint8_t data)
{
  uint8_t dataOut[2];
  uint8_t dataIn[2];

  dataOut[0] = SET_WRITE_CMD(reg);
  dataOut[1] = data;
  
  /* start spi comunication by pullind down ChipSelect line */
  SPI_CS_ACTIVE;
  /* start spi comunication by pullind down ChipSelect line */
  TM_SPI_SendMulti(SPI2,(uint8_t*)&dataOut,(uint8_t*)&dataIn,2);

  /* Wait for transmission to complete */
  while(SPI_IS_BUSY(SPI2));

  /* terminate transmission, Chip select pushed up*/
  SPI_CS_INACTIVE;
}

uint8_t SPI_READ_REG(uint8_t reg)
{
  uint8_t dataOut[2];
  uint8_t dataIn[2];

  dataOut[0] = SET_READ_CMD(reg);
  dataOut[1] = 0;
  
  /* start spi communication by pullind down ChipSelect line */
  SPI_CS_ACTIVE;
  /* ... */
  TM_SPI_SendMulti(SPI2,(uint8_t*)&dataOut,(uint8_t*)&dataIn,2);

  /* Wait for transmission to complete */
  while(SPI_IS_BUSY(SPI2));

  /* terminate transmission, Chip select pushed up*/
  SPI_CS_INACTIVE;
  return dataIn[1];
}

void SPI_READ_MULT_REGS(uint8_t reg, uint8_t *data, uint8_t Count)
{
  uint8_t dataOut[2];
  uint8_t *dataIn;

  Count++;
  dataIn = (uint8_t*)malloc(Count);
  dataOut[0] = SET_READ_CMD(reg);
  dataOut[1] = 0;

  /* start spi comunication by pullind down ChipSelect line */
  SPI_CS_ACTIVE;
  /* start spi comunication by pullind down ChipSelect line */
  TM_SPI_SendMulti(SPI2,(uint8_t*)&dataOut,dataIn,Count);

  /* Wait for transmission to complete */
  while(SPI_IS_BUSY(SPI2));

  /* terminate transmission, Chip select pushed up*/
  SPI_CS_INACTIVE;

  memcpy((void*)data,(void*)++dataIn,Count);
}

/**
 * @brief      { function_description }
 *
 * @return     { description_of_the_return_value }
 */
MAX31865_Result_t MAX31865_Init(void)
{

  /* Enable Chip Select pin as output, CS High inactive */
  TM_GPIO_Init(GPIOB,GPIO_Pin_12,TM_GPIO_Mode_OUT,TM_GPIO_OType_PP,TM_GPIO_PuPd_UP, TM_GPIO_Speed_High);
  SPI_CS_INACTIVE;

  /* Enable Data Ready line as input, goes low when a new conversion result is available in the data register. */
  TM_GPIO_Init(GPIOB,GPIO_Pin_11,TM_GPIO_Mode_IN,TM_GPIO_OType_PP,TM_GPIO_PuPd_NOPULL,TM_GPIO_Speed_High );

  /* initialize SPI module as master, mode 3 */
  TM_SPI_InitFull(SPI2, TM_SPI_PinsPack_2, SPI_BAUDRATEPRESCALER_128, TM_SPI_Mode_3, SPI_MODE_MASTER, SPI_FIRSTBIT_MSB);

  debug_println("SPI module init successfully");

  /* Write configuration register, it turns on Vbias, so it's able to take measures */
  SPI_WRITE_REG(CONFIG_REGISTER, CONFIG_VBIAS );

  debug_print("Config register = 0x%X",SPI_READ_REG(CONFIG_REGISTER));

  return MAX31865_Result_Ok;
}

MAX31865_Result_t MAX31865_config(uint8_t value)
{
  SPI_WRITE_REG(CONFIG_REGISTER, value);
  return MAX31865_Result_Ok;
}


MAX31865_Result_t MAX31865_Read_Data(uint16_t *RTD_data)
{
  uint8_t dataIn[2];

  /* Read out RTD value registers that will force the chip deliver new RTD value*/
  SPI_READ_MULT_REGS(RTD_MSB_REGISTER,(uint8_t*)&dataIn,2);
  /*This reading is garbage, need to clear it off.*/
  memset((void*)&dataIn,0,sizeof(dataIn));
  
  /* turning on Vbias and enable one shot conversion mode */
  SPI_WRITE_REG(CONFIG_REGISTER, CONFIG_VBIAS | CONFIG_ONE_SHOT);

  //debug_print("Config register = 0x%X",SPI_READ_REG(CONFIG_REGISTER));

  // wait until Data ready line goes low.
  while(TM_GPIO_GetInputPinValue(GPIOB,GPIO_Pin_11));

  /*Read data out from register */
  SPI_READ_MULT_REGS(RTD_MSB_REGISTER,(uint8_t*)&dataIn,2);

  *RTD_data = ((dataIn[0]<<8) | (dataIn[1]))>>1;

  return (dataIn[1]&RTD_REGISTER_FAULT) ? MAX31865_Result_Error : MAX31865_Result_Ok;
}

uint8_t MAX31865_Fault_Detection(void)
{
  uint32_t Delay = 0x5B8D80;
  uint8_t Done = false;
  uint8_t status = 0;
  debug_println("Fault detection test started");
  SPI_WRITE_REG(CONFIG_REGISTER, CONFIG_VBIAS | FAULT_DETECTION_AUTO_DELAY);

  do
  {
    debug_println("waiting until fault detection is done");
    while(Delay--);
    if( (SPI_READ_REG(CONFIG_REGISTER)&CONFIG_FAULT_DETECTION) == FAULT_DETECTION_FINISHED )
    {
      debug_println("Fault detection done");
      Done = true;
    }
    Delay = 0x5B8D80;
  }while(Done != true);
  status = SPI_READ_REG(FAULT_STATUS_REGISTER);
  debug_print("Fault Status = 0x%02X",status);

  return status;
}

MAX31865_Result_t MAX31865_Fault_clear(void)
{
  debug_println("Clearing fault");
  SPI_WRITE_REG(CONFIG_REGISTER, CONFIG_VBIAS | CONFIG_FAULT_STATUS_CLEAR);
  return MAX31865_Result_Ok;
}

/**
 * ------------------------------------------------------------------------------------------------------
 */
MAX31865_Result_t MAX31865_Read_Temperature(float *RTD_Temp)
{
  uint16_t data;
  float RTD_resistence;

  if (MAX31865_Read_Data((uint16_t*)&data) != MAX31865_Result_Ok)
  {
    return MAX31865_Result_Error;
  }
  RTD_resistence = CAL_RTD_RESISTANCE(data);
  *RTD_Temp = RTD_CAL_getTemperature(RTD_resistence);

  debug_println("RTD register = %d", data);
  debug_println("RTD resistance = %f", RTD_resistence);
  debug_println("Temperature = %f",*RTD_Temp);

  return MAX31865_Result_Ok;
}




