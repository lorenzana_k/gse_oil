/**
 * @author  
 * @email   
 * @website 
 * @link    
 * @version 
 * @ide     
 * @license 
 * @brief   
 *  
\verbatim
   ----------------------------------------------------------------------
    Copyright (c) 2017

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without restriction,
    including without limitation the rights to use, copy, modify, merge,
    publish, distribute, sublicense, and/or sell copies of the Software, 
    and to permit persons to whom the Software is furnished to do so, 
    subject to the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
    AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
   ----------------------------------------------------------------------
\endverbatim
 */
#ifndef MAX31865_H
#define MAX31865_H

/* C++ detection */
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @addtogroup TM_STM32Fxxx_HAL_Libraries
 * @{
 */

/**
 * @defgroup MAX31865
 * @brief    
 * @{
 *
 */
#include "tm_stm32_spi.h"

/**
 * @defgroup MAX31865_Macros
 * @brief    Library defines
 * @{
 */
/**
 * @brief MAX31865 Register addresses
 */
#define CONFIG_REGISTER           0x00  /*!< Configuration Register */  
#define RTD_MSB_REGISTER          0x01  /*!< */  
#define RTD_LSB_REGISTER          0x02  /*!< */  
#define HI_FAULT_TH_MSB_REGISTER  0x03  /*!< */  
#define HI_FAULT_TH_LSB_REGISTER  0x04  /*!< */  
#define LO_FAULT_TH_MSB_REGISTER  0x05  /*!< */  
#define LO_FAULT_TH_LSB_REGISTER  0x06  /*!< */  
#define FAULT_STATUS_REGISTER     0x07  /*!< */  

/*Configuration Register */  
#define CONFIG_VBIAS                  0x80  /*!< VBIAS, 1 = ON, 0 = OFF*/
#define CONFIG_CONVERSION_MODE_AUTO   0x40  /*!< Conversion mode 1 = Auto 0 = Normally off*/
#define CONFIG_ONE_SHOT               0x20  /*!< 1-shot 1 = 1-shot (auto-clear)*/
#define CONFIG_3_WIRE_RTD             0x10  /*!< 3-wire 1 = 3-wire RTD 0 = 2-wire or 4-wire*/
#define CONFIG_FAULT_DETECTION        0x0C
#define CONFIG_FAULT_DETECTION_HI     0x08  /*!< Fault Detection Cycle Control*/
#define CONFIG_FAULT_DETECTION_LO     0x04  /*!< */
#define CONFIG_FAULT_STATUS_CLEAR     0x02  /*!< Fault Status Clear 1 = Clear (auto-clear)*/
#define CONFIG_FILTER_SELECT          0x01  /*!< 50/60Hz filter select 1 = 50Hz 0 = 60Hz*/

#define FAULT_DETECTION_NO_ACTION       0x00
#define FAULT_DETECTION_AUTO_DELAY      0x04
#define FAULT_DETECTION_MANUAL_CYCLE_1  0x08
#define FAULT_DETECTION_MANUAL_CYCLE_2  0x0C

#define FAULT_DETECTION_FINISHED        0x00
#define FAULT_DETECTION_AUTO_RUNNING    0x04
#define FAULT_DETECTION_WAIT_CYCLE_1    0x08
#define FAULT_DETECTION_WAIT_CYCLE_2    0x0C

#define RTD_REGISTER_FAULT              0x01
  

/*Read/Write bit*/


 /**
 * @}
 */
 
/**
 * @defgroup MAX31865_Typedefs
 * @brief    Library Typedefs
 * @{
 */



/**
 * @brief  I2C result enumeration
 */
typedef enum {
    MAX31865_Result_Ok = 0x00, /*!< Everything OK */
    MAX31865_Result_Error      /*!< An error has occurred */
} MAX31865_Result_t;




/**
 * @}
 */

/**
 * @defgroup 
 * @brief    Library Functions
 * @{
 */

/**
 * @brief  Initializes I2C peripheral
 * @param  *I2Cx: Pointer to I2Cx peripheral you will use for iintialization
 * @param  pinspack: Pinspack used for GPIO initialization. This parameter can be a value of @ref TM_I2C_PinsPack_t enumeration
 * @param  clockSpeed: Clock speed in units of Hz for I2C communication
 * @retval Member of @ref TM_I2C_Result_t enumeration
 */
MAX31865_Result_t MAX31865_Init(void);

MAX31865_Result_t MAX31865_config(uint8_t value);
/**
 * @brief  Initializes I2C peripheral
 * @param  *I2Cx: Pointer to I2Cx peripheral you will use for iintialization
 * @param  pinspack: Pinspack used for GPIO initialization. This parameter can be a value of @ref TM_I2C_PinsPack_t enumeration
 * @param  clockSpeed: Clock speed in units of Hz for I2C communication
 */
MAX31865_Result_t MAX31865_Reset(void);

/**
 * @brief  
 */
MAX31865_Result_t MAX31865_Read_Data(uint16_t *RTD_data);

/**
 * @brief  
 */
uint8_t MAX31865_Fault_Detection(void);

/**
 * @brief  
 */
MAX31865_Result_t MAX31865_Fault_clear(void);

/**
 * @brief  
 */
MAX31865_Result_t MAX31865_Read_Temperature(float *RTD_Temp);



/**
 * @}
 */
 
/**
 * @}
 */
 
/**
 * @}
 */

/* C++ detection */
#ifdef __cplusplus
}
#endif

#endif

