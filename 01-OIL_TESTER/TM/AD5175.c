/** 
 * @author  
 * @email   
 * @website 
 * @link    
 * @version 
 * @ide     
 * @license 
 * @brief   
 *
\verbatim
   ----------------------------------------------------------------------
    Copyright (c) 2016 

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without restriction,
    including without limitation the rights to use, copy, modify, merge,
    publish, distribute, sublicense, and/or sell copies of the Software, 
    and to permit persons to whom the Software is furnished to do so, 
    subject to the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
    AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
   ----------------------------------------------------------------------
\endverbatim
 */
#include "AD5175.h"
#include "RTD_CAL.h"
#include "stm32f070xb.h"
#include "string.h"
#include "stm32_flash.h"

#define AD5175_I2C_ADDRESS                 0x5E

#define R_WIPER                (float)35   /*  Wiper resistance */
#define RH_STEPS             (float)1024   /*  Digital poisistion on rheostat */
#define REF_RESISTANCE  (float)10000   /* AD5175 has 10kOhms nominal resistance*/

#define R_PARALLEL              (float)2000

#define RTD_MAX_ALLOWABLE       CAL_TOTAL_RESISTACE(NOMINAL_RESISTANCE)
/**
 * @brief      Calculates      
 */
#define CAL_RHEOSTAT_RESISTACE(Req)   \
    (float)  ( (R_PARALLEL*((float)Req)) / (R_PARALLEL-((float)Req)) )

/**
 * @brief      Calculates      
 */
#define CAL_TOTAL_RESISTACE(R1)   \
    (float)  ( (R_PARALLEL*((float)R1)) / (R_PARALLEL+((float)R1)) ) 

/**
 * @brief          
 */
#define GET_DIGITAL_CODE_RHEOSTAT(Rrheostat)   \
    (uint16_t)  round(( (((float)Rrheostat)*RH_STEPS)/NOMINAL_RESISTANCE ))

/**
 * @brief          
 */
#define GET_RHEOSTAT_RESISTANCE(RDAC)   \
    (float)  ( (((float)RDAC)*NOMINAL_RESISTANCE)/RH_STEPS ) 

//const float EndToEndResistance = (float)10000.0;
//float NOMINAL_RESISTANCE = (float)10000;
/**
 * @brief      initializes the digital rheostat
 *
 * @return     
 */
AD5175_Result_t AD5175_Init(void)
{
  uint16_t data;

  /**
   * On power up serial data input register write commands for RDAC and TP are disables.
   * it needs to be enable by setting RDAC in the control register.
   */
  AD5175_FORM_FRAME(AD5175_WRITE_CR, AD5175_CR_RDAC_ENABLE, data );

  if (TM_I2C_WriteMultiNoRegister(I2C1,AD5175_I2C_ADDRESS ,(uint8_t*)&data, 2) == TM_I2C_Result_Ok){
      debug_println("Temperature simulator Initialize successfully!");
  } else {
      debug_println("Temperature simulator Error!");

      return AD5175_Result_Error;
  }

/*
  if( AD5175_ReadCalib((float*) &NOMINAL_RESISTANCE) == TM_I2C_Result_Ok)
  {
    debug_println("Calibration successful");
  }
*/
  return AD5175_Result_Ok;
}

AD5175_Result_t AD5175_Reset(void)
{
  uint16_t data;
  /* sending software reset command */
  AD5175_FORM_FRAME(AD5175_SW_RESET, 0, data );

  if (TM_I2C_WriteMultiNoRegister(I2C1,AD5175_I2C_ADDRESS ,(uint8_t*)&data, 2) != TM_I2C_Result_Ok){
      return AD5175_Result_Error;
  }  
  return AD5175_Result_Ok;
}

AD5175_Result_t AD5175_WriteWiper(uint16_t Wiper)
{
  uint16_t data;

  /* Wiper value is bigger that the maximum permitted */
  if ( Wiper > AD5175_RDAC_MAX)
  {
    return AD5175_Result_Error;
  }
  /**
   * Stetting Wiper resister at the minimum value.
   */
  AD5175_FORM_FRAME(AD5175_WRITE_RDAC, Wiper, data );

  if (TM_I2C_WriteMultiNoRegister(I2C1,AD5175_I2C_ADDRESS ,(uint8_t*)&data, 2) != TM_I2C_Result_Ok){
      return AD5175_Result_Error;
  }

  return AD5175_Result_Ok;
}

AD5175_Result_t AD5175_ReadWiper(uint16_t *Wiper)
{
  uint16_t data;
  /* Creating frame to read wiper resistance register */
  AD5175_FORM_FRAME(AD5175_READ_RDAC, 0, data );

  if (TM_I2C_WriteMultiNoRegister(I2C1,AD5175_I2C_ADDRESS ,(uint8_t*)&data, 2) != TM_I2C_Result_Ok){
      return AD5175_Result_Error;
  } 

  if (TM_I2C_ReadMultiNoRegister(I2C1,AD5175_I2C_ADDRESS ,(uint8_t*)&data, 2) != TM_I2C_Result_Ok){
      return AD5175_Result_Error;
  } 

  /* Copy wiper value to pointer*/
  RETURN_WIPER(Wiper,data);

  return AD5175_Result_Ok;
}

AD5175_Result_t AD5175_ReadCalib(float* data)
{
  uint16_t i2c_cmd;
  uint8_t i2c_input_data[2];
  float Tolerance;
  float Resistance;
  uint8_t Tolerance_MSB;
  uint8_t Tolerance_LSB;

  //TM_I2C_ReadMulti(I2C_TypeDef* I2Cx, uint8_t device_address, uint8_t register_address, uint8_t* data, uint16_t count);

  AD5175_FORM_FRAME(AD5175_READ_TPM, 0x39,i2c_cmd);

  if (TM_I2C_WriteMultiNoRegister(I2C1,AD5175_I2C_ADDRESS ,(uint8_t*)&i2c_cmd, 2) != TM_I2C_Result_Ok){
      return AD5175_Result_Error;
  } 

  if (TM_I2C_ReadMultiNoRegister(I2C1,AD5175_I2C_ADDRESS ,(uint8_t*)&i2c_input_data,2) != TM_I2C_Result_Ok){
      return AD5175_Result_Error;
  } 
  debug_println("Calib reg 0x39 = %04X",(uint16_t)(i2c_input_data[0]<<8 | i2c_input_data[1]) );
  Tolerance_MSB = i2c_input_data[1];
  memset((void*)&i2c_input_data,0,sizeof(i2c_input_data));

  AD5175_FORM_FRAME(AD5175_READ_TPM, 0x3A,i2c_cmd);

  if (TM_I2C_WriteMultiNoRegister(I2C1,AD5175_I2C_ADDRESS ,(uint8_t*)&i2c_cmd, 2) != TM_I2C_Result_Ok){
      return AD5175_Result_Error;
  } 

  if (TM_I2C_ReadMultiNoRegister(I2C1,AD5175_I2C_ADDRESS ,(uint8_t*)&i2c_input_data,2) != TM_I2C_Result_Ok){
      return AD5175_Result_Error;
  } 
  debug_println("Calib reg 0x3A = %04X",(uint16_t)(i2c_input_data[0]<<8 | i2c_input_data[1]));
  Tolerance_LSB = i2c_input_data[1];
  memset((void*)&i2c_input_data,0,sizeof(i2c_input_data));

  /* Calculate end to end resistance according to Tolerance stored on chip.*/
  Tolerance = (float) (TOLERANCE_MASK_INT&Tolerance_MSB);
  Tolerance += (float) ((TOLERANCE_MASK_FRAC&Tolerance_LSB)*CONST_POW_2_8);
  Tolerance *= (float)100.0;
    if( !(Tolerance_MSB & TOLERANCE_MASK_SIGN))
  {
      Resistance = REF_RESISTANCE - Tolerance;
  }
  else
  {
      Resistance = REF_RESISTANCE + Tolerance;
  }
  debug_println("Tolerance = %f%%",Tolerance/100.0);
  
  memcpy((void*)data,(void*)&Resistance,sizeof(Tolerance)); 
  return AD5175_Result_Ok;
}
/**
 * ------------------------------------------------------------------------------------------------------
 */

AD5175_Result_t AD5175_set_RTD(float Temperature)
{
  float Rt;
  float R_rheostat;
  float R_digital;

  /* 1) Get equivalente RTD resistance for given temperature (Rt). */
  Rt = RTD_CAL_getResistance(Temperature);

  if (RTD_MAX_ALLOWABLE <= Rt)
  {
    debug_println("Temperature is above limit");
    return AD5175_Result_Error;
  }
  /* 2) Calculate Rrheostat, Rt is equivalente to parallel resistance calculated 10KOhms || Rrheostat. */
  R_rheostat = CAL_RHEOSTAT_RESISTACE((float)Rt);
  /* 3) Get digital code for Rwiper given by D = ((Rrheostat - 35Ohms)*1024)/10KOhms. */
  R_digital = GET_DIGITAL_CODE_RHEOSTAT(R_rheostat);

  debug_println("Temperature  = %f",Temperature);
  debug_println("Resistor on PT1000 = %f",Rt);
  debug_println("Resistor on rheostat = %f",R_rheostat);
  debug_println("Rheostat digital value = %f",R_digital);

  /* Writes values of rheastat wiper */
  if(AD5175_WriteWiper(R_digital) != AD5175_Result_Ok)
  {
    return AD5175_Result_Error;
  }

  return AD5175_Result_Ok;
}

AD5175_Result_t AD5175_get_RTD(float *Temperature)
{
  uint16_t RDAC;
  float rheostat_data;
  float equivalent_resisntace;

  /* 1) reads value of rheostat wiper */
  if (AD5175_ReadWiper((uint16_t*)&RDAC) != AD5175_Result_Ok)
  {
    return AD5175_Result_Error;
  }
  /* 2) calculates the resistance on rheostat */
  rheostat_data = GET_RHEOSTAT_RESISTANCE(RDAC);
  /* 2) Calculates resistance in parallel*/
  equivalent_resisntace = CAL_TOTAL_RESISTACE(rheostat_data);
  /* 3) calculates Temperature with given parallel resistance */
  *Temperature = RTD_CAL_getTemperature((float) equivalent_resisntace);

  debug_println("Rheostat digital value = %d",RDAC);  
  debug_println("Resistor on rheostat = %f",rheostat_data);
  debug_println("Resistor on PT1000 = %f",equivalent_resisntace);
  debug_println("Temperature  = %f",*Temperature);

  return AD5175_Result_Ok;
}
