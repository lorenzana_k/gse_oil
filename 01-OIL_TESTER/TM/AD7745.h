/**
 * @author  
 * @email   
 * @website 
 * @link    
 * @version 
 * @ide     
 * @license 
 * @brief   
 *  
\verbatim
   ----------------------------------------------------------------------
    Copyright (c) 2017

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without restriction,
    including without limitation the rights to use, copy, modify, merge,
    publish, distribute, sublicense, and/or sell copies of the Software, 
    and to permit persons to whom the Software is furnished to do so, 
    subject to the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
    AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
   ----------------------------------------------------------------------
\endverbatim
 */
#ifndef AD77545_H
#define AD77545_H

/* C++ detection */
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @addtogroup TM_STM32Fxxx_HAL_Libraries
 * @{
 */

/**
 * @defgroup AD7745
 * @brief    
 * @{
 *
 */
#include "tm_stm32_i2c.h"

/**
 * @defgroup AD7745_Macros
 * @brief    Library defines
 * @{
 */
/**
 * @brief AD7745 Register addresses
 */
#define STATUS          0x00    /*!< R*/     
#define CAP_DATA_H      0x01    /*!< R*/  
#define CAP_DATA_M      0x02    /*!< R*/  
#define CAP_DATA_L      0x03    /*!< R*/  
#define VT_DATA_H       0x04    /*!< R*/   
#define VT_DATA_M       0x05    /*!< R*/   
#define VT_DATA_L       0x06    /*!< R*/   
#define CAP_SETUP       0x07    /*!< R/W*/   
#define VT_SETUP        0x08    /*!< R/W*/    
#define EXC_SETUP       0x09    /*!< R/W*/   
#define CONFIGURATION   0x0A    /*!< R/W*/   
#define CAP_DAC_A       0x0B    /*!< R/W*/   
#define CAP_DAC_B       0x0C    /*!< R/W*/   
#define CAP_OFFSET_H    0x0D    /*!< R/W*/    
#define CAP_OFFSET_L    0x0E    /*!< R/W*/    
#define CAP_GAIN_H      0x0F    /*!< R/W*/  
#define CAP_GAIN_L      0x10    /*!< R/W*/  
#define VOLT_GAIN_H     0x11    /*!< R/W*/ 
#define VOLT_GAIN_L     0x12    /*!< R/W*/ 

#define AD7745_REGISTERS_AMT   19  /* Chip has 19 I2C registers */

/* Status register Mnemonics */
#define STAT_EXCERR         ((uint8_t)0x08) 
#define STAT_RDY            ((uint8_t)0x04)
#define STAT_RDYVT          ((uint8_t)0x02)
#define STAT_RDYCAP         ((uint8_t)0x01)

/* Cap Setup register Mnemonics */
#define CAPSET_CAPEN        ((uint8_t)0x80)
#define CAPSET_CAPDIFF      ((uint8_t)0x20)
#define CAPSET_CAPCHOP      ((uint8_t)0x01)

/* VT Setup register Mnemonics */
#define VTSET_VTEN          ((uint8_t)0x80)
#define VTSET_VTMD1         ((uint8_t)0x40)
#define VTSET_VTMD0         ((uint8_t)0x20)
#define VTSET_EXTREF        ((uint8_t)0x10)
#define VTSET_VTSHORT       ((uint8_t)0x02)
#define VTSET_VTCHOP        ((uint8_t)0x01)

/* EXC Setup register Mnemonics */
#define EXCSET_CLKCTRL      ((uint8_t)0x80)
#define EXCSET_EXCON        ((uint8_t)0x40)
#define EXCSET_EXCB         ((uint8_t)0x20)
#define EXCSET_EXCB_NEG     ((uint8_t)0x10)
#define EXCSET_EXCA         ((uint8_t)0x08)
#define EXCSET_EXCA_NEG     ((uint8_t)0x04)
#define EXCSET_EXCLVL1      ((uint8_t)0x02)
#define EXCSET_EXCLVL0      ((uint8_t)0x01)

/* Configuration register Mnemonics */
#define CONFIG_VTFS1        ((uint8_t)0x80)
#define CONFIG_VTFS0        ((uint8_t)0x40)
#define CONFIG_CAPFS2       ((uint8_t)0x20)
#define CONFIG_CAPFS1       ((uint8_t)0x10)
#define CONFIG_CAPFS0       ((uint8_t)0x08)
#define CONFIG_MD2          ((uint8_t)0x04)
#define CONFIG_MD1          ((uint8_t)0x02)
#define CONFIG_MD0          ((uint8_t)0x01)

#define IDLE_MODE                       ((uint8_t)0x00)
#define CONT_CONVERSION_MODE            ((uint8_t)0x01)
#define SINGLE_CONVERSION_MODE          ((uint8_t)0x02)
#define POWER_DOWN_MODE                 ((uint8_t)0x03)
#define CAP_SYSTEM_OFFSET_CAL_MODE      ((uint8_t)0x05)
#define CAP_VOL_SYSTEM_GAIN_CAL_MODE    ((uint8_t)0x06)


/* Cap DAC A register Mnemonics */
#define DAC_A_DACAENA       ((uint8_t)0x80)
#define DAC_A_DACA          ((uint8_t)0x7F)

/* Cap DAC B register Mnemonics */
#define DAC_B_DACBENB       ((uint8_t)0x80)
#define DAC_B_DACB          ((uint8_t)0x7F)


 /**
 * @}
 */
 
/**
 * @defgroup AD7745_Typedefs
 * @brief    Library Typedefs
 * @{
 */

typedef struct 
{
    uint16_t rawCapacitiveOffsetCalibration;
    uint16_t rawCapacitiveGainCalibration;
    float C_CAPDAC;
}AD7745_cal;

typedef struct
{
    uint16_t CapDAC_A;
    uint16_t CapDAC_B;
}AD7745_CAP_DAC;

typedef struct
{
    uint32_t SampleBuffer[100];
    double    CapBuffer[100];
    uint32_t* WrPt;
    uint8_t PendingSamples;
}AD7745_Samples;

extern const AD7745_cal factory_cal;
extern const AD7745_CAP_DAC Cap_DAC;
/**
 * @brief  I2C result enumeration
 */
typedef enum {
    AD7745_Result_Ok = 0x00, /*!< Everything OK */
    AD7745_Result_Error      /*!< An error has occurred */
} AD7745_Result_t;


/**
 * @brief  IAD7745 Register map
 */
typedef struct {
    uint8_t Status;                 /*!< This register indicates the status of the converter                       */
    uint8_t Cap_Data_H;             /*!< Capacitive channel output data                                            */
    uint8_t Cap_Data_M;             /*!< Voltage-Temperature channel output data                                   */
    uint8_t Cap_Data_L;             /*!<                                                                           */       
    uint8_t VT_Data_H;              /*!<                                                                           */    
    uint8_t VT_Data_M;              /*!<                                                                           */
    uint8_t VT_Data_L;              /*!<                                                                           */       
    uint8_t Cap_Setup;              /*!<                                                                           */
    uint8_t VT_Setup;               /*!<                                                                           */
    uint8_t EXC_Setup;              /*!<                                                                           */
    uint8_t Configuration;          /*!<                                                                           */
    uint8_t Cap_DAC_A;              /*!<                                                                           */    
    uint8_t Cap_DAC_B;              /*!<                                                                           */
    uint8_t Cap_Offset_H;           /*!<                                                                           */
    uint8_t Cap_Offset_L;           /*!<                                                                           */
    uint8_t Cap_Gain_H;             /*!<                                                                           */
    uint8_t Cap_Gain_L;             /*!<                                                                           */
    uint8_t Volt_Gain_H;            /*!<                                                                           */
    uint8_t Volt_Gain_L;            /*!<                                                                           */
} AD7745_Registers;


/**
 * @}
 */

/**
 * @defgroup 
 * @brief    Library Functions
 * @{
 */

/**
 * @brief  Initializes I2C peripheral
 * @param  *I2Cx: Pointer to I2Cx peripheral you will use for iintialization
 * @param  pinspack: Pinspack used for GPIO initialization. This parameter can be a value of @ref TM_I2C_PinsPack_t enumeration
 * @param  clockSpeed: Clock speed in units of Hz for I2C communication
 * @retval Member of @ref TM_I2C_Result_t enumeration
 */
AD7745_Result_t AD7745_Init(void);


/**
 * @brief  Initializes I2C peripheral
 * @param  *I2Cx: Pointer to I2Cx peripheral you will use for iintialization
 * @param  pinspack: Pinspack used for GPIO initialization. This parameter can be a value of @ref TM_I2C_PinsPack_t enumeration
 * @param  clockSpeed: Clock speed in units of Hz for I2C communication
 */
 AD7745_Result_t AD7745_Reset(void);

 AD7745_Result_t AD7745_Read_Data(uint32_t* CapData);

 AD7745_Result_t AD7745_Take_Samples(void);

 AD7745_Result_t AD7745_ReadCalib(AD7745_cal* CapDac);

/**
 * @}
 */
 
/**
 * @}
 */
 
/**
 * @}
 */

/* C++ detection */
#ifdef __cplusplus
}
#endif

#endif

