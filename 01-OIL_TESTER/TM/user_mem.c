#include "AD7745.h"
#include "AD5175.h"

#pragma push
#pragma Ono_remove_unused_constdata
const AD7745_cal factory_cal;
#pragma pop

#pragma push
#pragma Ono_remove_unused_constdata
const AD7745_CAP_DAC Cap_DAC;
#pragma pop

#pragma push
#pragma Ono_remove_unused_constdata
const float NOMINAL_RESISTANCE;
#pragma pop
