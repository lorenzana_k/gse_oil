/**
 * Keil project example for ESP8266 SERVER mode without RTOS
 *
 * @note      Check defines.h file for configuration settings!
 * @note      When using Nucleo F411 board, example has set 8MHz external HSE clock!
 *
 * Before you start, select your target, on the right of the "Load" button
 *
 * @author    Tilen Majerle
 * @email     tilen@majerle.eu
 * @website   http://stm32f4-discovery.net
 * @ide       Keil uVision 5
 * @conf      PLL parameters are set in "Options for Target" -> "C/C++" -> "Defines"
 * @packs     STM32F4xx/STM32F7xx Keil packs are requred with HAL driver support
 * @stdperiph STM32F4xx/STM32F7xx HAL drivers required
 *
 * \par Description
 *
 * This examples shows how you can use ESP for basic server
 *
 * - Library is initialized using ESP_Init
 * - Device must connect to network. Check WIFINAME and WIFIPASS defines for proper settings for your wifi network
 * - On debug port, IP address will be written to you where you can connect with browser
 * - Magic will begin, you should see something on your screen on PC
 * - On debug output (PA2 pin) is debug_println targeted via UART at 921600 bauds
 *
 * \note  Example uses separate buffers for each connection, because multiple connections can be active at a time
 *
 * \par Pinout for example (Nucleo STM32F411)
 *
\verbatim
ESP         STM32F4xx           DESCRIPTION
 
RX          PA9                 TX from STM to RX from ESP
TX          PA10                RX from STM to RX from ESP
VCC         3.3V                Use external 3.3V regulator
GND         GND
RST         PA0                 Reset pin for ESP
CTS         PA3                 RTS from ST to CTS from ESP
            BUTTON(PA0, PC13)   Discovery/Nucleo button, depends on configuration
            
            PA2                 TX for debug purpose (connect to PC) with 921600 bauds
\endverbatim
 */
/* Include core modules */
#include "stm32fxxx_hal.h"
/* Include my libraries here */
#include "defines.h"
#include "tm_stm32_disco.h"
#include "tm_stm32_delay.h"
#include "tm_stm32_usart.h"
#include "tm_stm32_i2c.h"
#include "esp8266.h"
#include "RS485.h"
#include "main.h"


#include "EventsEngine.h"
#include "command-interpreter.h"


/* Set debug port */
#define DEBUG_USART         DISCO_USART
#define DEBUG_USART_PP      DISCO_USART_PP

/* Wifi network settings, replace with your settings */
#define WIFINAME            "NETGEAR68"
#define WIFIPASS            "happyjungle309"

/* Set up your server settings to connect to */
#define IP_ADDR             "192.168.1.6"
#define PORT                5000

/* ESP working structure and result enumeration */
evol ESP_t ESP;
ESP_Result_t espRes;

/* Client connection pointer */
ESP_CONN_t* conn;

/* Connection manupulation */
uint32_t bw;
const uint8_t responseData[] = ""
"HTTP/1.1 200 OK\r\n"
"Content-Type: text/html\r\n"
"Connection: close\r\n"
"\r\n"
"<html>\n"
"   <head>\n"
"       <meta http-equiv=\"Refresh\" content=\"1\" />\n"
"   </head>\n"
"   <body>\n"
"       <h1>Welcome to web server produced by ESP8266 Wi-Fi module!</h1>\n"
"       This website will constantly update itself every 1 second!\n"
"   </body>\n"
"</html>\n";

const uint8_t responseData2[] = "Hello Host.";


EventControl ESP_stackControl; 
EventControl ESP_InitEventControl; 

      
void ESP_stackFunction(void);  
void ESP_InitEventFunction(void); 

/* ESP callback declaration */
int ESP_Callback(ESP_Event_t evt, ESP_EventParams_t* params);



void CAP_DataRdyEvent(void);

/* Periodic timer event*/
EventControl BlinkEventControl;
void BlinkEventFunction(void);

void OilSensorDataReceived(RS485_Response_Buffer data);




int main(void) {
    TM_RCC_InitSystem();                                    /* Init system */
    HAL_Init();                                             /* Init HAL layer */
    TM_DISCO_LedInit();                                     /* Init leds */
    TM_DISCO_ButtonInit();                                  /* Init button */
    TM_DELAY_Init();                                        /* Init delay */
    TM_USART_Init(DEBUG_USART, DEBUG_USART_PP, 921600);     /* Init USART for debug purpose */

    /* Print first screen message */
    debug_println("ESP8266 commands parser; Compiled: %s %s", __DATE__, __TIME__);
    
    emberCommandReaderInit();                               /* Init CLI */
    emberCommandInterpreterEchoOn();
    EventInit();
    

    if(RS485_Init() == RS485_Result_Ok )
    {
        debug_println("RS485 init successfully!");
    }
    else
    {
        debug_println("RS485 Init error!");
    }
   
    EventControlSetDelayMS(BlinkEventControl, 500);

    EventControlSetActive(ESP_stackControl);

    if ( (SSID[0] != '\0') && (Password[0] != '\0') )
    {
        EventControlSetDelayMS(ESP_InitEventControl,10000);
    }else{
        debug_println("WLAN not set");
    } 
    
    char buff[32];
    debug_println("Setting 485 ID");
    sprintf(buff, "$00SA%02X", SensorID);
    RS485_sendCommand((uint8_t*)buff, strlen(buff),NULL);
  
    
#if 0
    if(MAX31865_Init() == MAX31865_Result_Ok )
    {
        debug_println("MAX31865 init successfully!");
    }
    else
    {
        debug_println("MAX31865 Init error!");
    }

    if(AD7745_Init() == AD7745_Result_Ok )
    {
        debug_println("AD7745 init successfully!");
    }
    else
    {
        debug_println("AD7745 Init error!");
    }
    
    if(AD5175_Init() == AD5175_Result_Ok )
    {
        debug_println("AD5175 init successfully!");
    }
    else
    {
        debug_println("AD5175 Init error!");
    }    
#endif
    
    while (1) 
    {
        
        RunEvents();

        if (emberProcessCommandInput(DISCO_USART)) {
          debug_print("\r\n%s>", ZA_PROMPT);
        }
      
    }
}

void ESP_InitEventFunction(void)
{
    EventControlSetInactive(ESP_InitEventControl);
    char Hi[32];

    if ( (SSID[0] == '\0') || (Password[0] == '\0') )
    {
        debug_println("Wifi SSID isn't set");
        EventControlSetDelayMS(ESP_InitEventControl,60000);
        return;
    }

    /* Init ESP library with 115200 bauds */
    if ((espRes = ESP_Init(&ESP, 115200, ESP_Callback)) == espOK) {
        debug_println("ESP module init successfully!");
    } else {
        debug_println("ESP Init error. Status: %d", espRes);
    }
    
    /* Try to connect to wifi network in blocking mode */
    if ((espRes = ESP_STA_Connect(&ESP, SSID, Password, NULL, 0, 1)) == espOK) {
        debug_println("Connected to network");
    } else {
        debug_println("Problems trying to connect to network: %d", espRes);
    }

    /* Try to connect to server as client, connect to example.com domain */
    if ((espRes = ESP_CONN_Start(&ESP, &conn, ESP_CONN_Type_TCP, ServerIP, socketPort, 1)) == espOK) {
        debug_println("Connected to %s:%d!", ServerIP, socketPort);

        sprintf(Hi, ":%02XHI\r\n", SensorID);
        wifiSendData((uint8_t*)Hi, strlen(Hi));

    } else {
        debug_println("Failed to connect: %d",espRes);

        EventControlSetDelayMS(ESP_InitEventControl,10000);
    }

}

/* Process ESP update */
void ESP_stackFunction(void)
{
    ESP_Update(&ESP);                                   
}


/***********************************************/
/**               Library callback            **/
/***********************************************/
int ESP_Callback(ESP_Event_t evt, ESP_EventParams_t* params) {
    ESP_CONN_t* conn;
    uint8_t* data;
    
    switch (evt) {                              /* Check events */
        case espEventIdle:
            debug_println("Stack is IDLE!");
            break;
        case espEventConnActive: {
            conn = (ESP_CONN_t *)params->CP1;   /* Get connection for event */
            debug_println("Connection %d just became active!", conn->Number);
            break;
        }
        case espEventConnClosed: {
            conn = (ESP_CONN_t *)params->CP1;   /* Get connection for event */
            debug_println("Connection %d was just closed!", conn->Number);

            EventControlSetDelayMS(ESP_InitEventControl,10000);
            break;
        }
        case espEventDataReceived: {
            conn = (ESP_CONN_t *)params->CP1;   /* Get connection for event */
            data = (uint8_t *)params->CP2;      /* Get data */
            
            /* Notify user about informations */
            debug_println("Data received: %d bytes", params->UI);
            debug_println("Data received: %.*s",params->UI, data);

            RS485_sendCommand(data, params->UI,OilSensorDataReceived);
            break;
        }
        case espEventDataSent:
            conn = (ESP_CONN_t *)params->CP1;   /* Get connection for event */
            debug_println("Data sent conn: %d", conn->Number);
            //debug_println("Close conn resp: %d", ESP_CONN_Close(&ESP, conn, 0));
            break;

        case espEventDataSentError:
            conn = (ESP_CONN_t *)params->CP1;   /* Get connection for event */
            ESP_CONN_Close(&ESP, conn, 0);
            break;
        default:
            break;
    }
    
    return 0;
}

void OilSensorDataReceived(RS485_Response_Buffer data)
{
  debug_println("Bytes received %d",data.ResponseSize);
  debug_println("response %s",data.ResponseData);
  debug_println("Sending response back via TCP/IP");
  wifiSendData(data.ResponseData,data.ResponseSize);
  memset((void*)&data,0,sizeof(data));
}

void wifiSendData(uint8_t* data, uint8_t dataSize)
{
    static uint8_t tempBuffer[32];
    static uint8_t size;

    memset((void*)&tempBuffer,0,sizeof(tempBuffer)); 
    memcpy((void*)&tempBuffer,(void*)data,dataSize);
    memcpy((void*)&size,(void*)&dataSize,sizeof(dataSize));

    if (ESP_IsReady(&ESP) == espOK) {   /* Send data back when we have received all the data from device */
        debug_println("Sending %d Byte%s\r\nPayload: %s",size,size>1?"s":"",tempBuffer);
        espRes = ESP_CONN_Send(&ESP, conn, tempBuffer, size, &bw, 0); /* Send data on other requests */
    }
}

void wifiConnect(void)
{
    if ( (SSID[0] != '\0') && (Password[0] != '\0') )
    {
        EventControlSetActive(ESP_InitEventControl);
    }else{
        debug_println("SSID and Password is not set");
    } 
}

/* 1ms handler function, called from SysTick interrupt */
void HAL_IncTick(void)
{
  /* Increase tick counter for HAL drivers */
  EventIT();
  ESP_UpdateTime(&ESP, 1);        /* Update ESP library time for 1 ms */
}

/* Periodic timer event*/
void BlinkEventFunction(void)
{
  EventControlSetDelayMS(BlinkEventControl, 500);
  //debug_println("Periodic event test\r\n");
  TM_DISCO_LedToggle(LED_GREEN);
}



/* debug_println handler */
int fputc(int ch, FILE* fil) {
    TM_USART_Putc(DEBUG_USART, ch);         /* Send over debug USART */
    return ch;                              /* Return OK */
}
