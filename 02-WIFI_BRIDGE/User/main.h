

#define SERVER_IP_SIZE		32
#define SSID_SIZE			32
#define PASSWORD_SIZE		64
#define SOCKET_PORT_SIZE	sizeof(uint32_t)
#define SENSOR_ID_SIZE		sizeof(uint8_t)

extern const uint32_t socketPort;
extern const char ServerIP[SERVER_IP_SIZE];
extern const char SSID[SSID_SIZE];
extern const char Password[PASSWORD_SIZE];
extern const char SensorID;


void wifiSendData(uint8_t* data, uint8_t dataSize);
void wifiConnect(void);
