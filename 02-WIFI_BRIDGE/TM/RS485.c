/** 
 * @author  
 * @email   
 * @website 
 * @link    
 * @version 
 * @ide     
 * @license 
 * @brief   
 *
\verbatim
   ----------------------------------------------------------------------
    Copyright (c) 2016 

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without restriction,
    including without limitation the rights to use, copy, modify, merge,
    publish, distribute, sublicense, and/or sell copies of the Software, 
    and to permit persons to whom the Software is furnished to do so, 
    subject to the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
    AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
   ----------------------------------------------------------------------
\endverbatim
 */
#include <stdlib.h>
#include <string.h>

#include "RS485.h"
//#include "stm32f070xb.h"
#include "stm32f071xb.h"
#include "tm_stm32_usart.h"

#include "EventsEngine.h"

#define RS485_USART     USART3
#define RS485_USART_PP  TM_USART_PinsPack_2

#define RS485_RE_PORT   GPIOB
#define RS485_RE_PIN    GPIO_Pin_2
#define RS485_RX_MODE   TM_GPIO_SetPinLow(RS485_RE_PORT,RS485_RE_PIN)  
#define RS485_TX_MODE   TM_GPIO_SetPinHigh(RS485_RE_PORT,RS485_RE_PIN)





typedef struct
{
    RS485_Response_Buffer Rsp;
    void (*CallBack)(RS485_Response_Buffer arg);
}RS485_Response_str;
 

EventControl RS485_handlerEventControl;
void RS485_handlerEventFunction(void); 

static RS485_Result_t myStatus = RS485_Result_PortNotInitialized;


static RS485_Response_str responseData;

RS485_Result_t RS485_Init(void)
{
    TM_USART_Init(RS485_USART, RS485_USART_PP, 9600);
    myStatus = RS485_Result_ReadyToSend;

    return RS485_Result_Ok;
}

RS485_Result_t RS485_sendCommand(uint8_t* OutputData, uint8_t OutputDataSize,void (*CB)(RS485_Response_Buffer response))
{

    /* Clear buffer, send data, it terminate with a carrier return, line feed sequence*/
    TM_USART_ClearBuffer(RS485_USART);
    TM_USART_Send(RS485_USART, OutputData, OutputDataSize );
    TM_USART_Putc(RS485_USART, '\r');
    TM_USART_Putc(RS485_USART, '\n');

    /* save pointer where response will be stored */
    responseData.CallBack = CB;
    /* Set status waiting for response*/
    myStatus =  RS485_Result_WaitingResponse;


    
    EventControlSetDelayMS(RS485_handlerEventControl, 200);

    return RS485_Result_Ok;
}


void RS485_handlerEventFunction(void)
{

    RS485_Response_Buffer *ResponseBuffer = (RS485_Response_Buffer*)&responseData.Rsp;

    EventControlSetInactive(RS485_handlerEventControl);

    responseData.Rsp.ResponseSize = TM_USART_Gets(RS485_USART,(char*)ResponseBuffer->ResponseData,RS485_BUFFER_SIZE);
    myStatus = RS485_Result_ReadyToSend;
    if (responseData.CallBack != NULL)
    {
        responseData.CallBack(responseData.Rsp);
    }
    memset((void*)&responseData,0,sizeof(responseData));

}

RS485_Result_t RS584_Status(void)
{
    return myStatus;
}
