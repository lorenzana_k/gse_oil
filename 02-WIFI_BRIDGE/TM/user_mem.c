#include <stdint.h>
#include "main.h"

#pragma push
#pragma Ono_remove_unused_constdata
const uint32_t socketPort;
#pragma pop

#pragma push
#pragma Ono_remove_unused_constdata
const char ServerIP[SERVER_IP_SIZE];
#pragma pop

#pragma push
#pragma Ono_remove_unused_constdata
const char SSID[SSID_SIZE];
#pragma pop

#pragma push
#pragma Ono_remove_unused_constdata
const char Password[PASSWORD_SIZE];
#pragma pop

#pragma push
#pragma Ono_remove_unused_constdata
const char SensorID;
#pragma pop



