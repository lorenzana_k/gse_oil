#include "string.h"
#include "command-interpreter.h"
#include "EventsEngine.h"
#include "stm32_flash.h"
#include "RS485.h"
#include "main.h"

void rs485_DataReceived(RS485_Response_Buffer data);

void version(void)
{
  debug_println("Oil Tester; Compiled: %s %s", __DATE__, __TIME__);
}

void sensorSetId(void)
{
  char buff[32];
  uint32_t data = (uint32_t)emberUnsignedCommandArgument(0);

  FLASH_write_entry((uint32_t)&SensorID,((uint32_t)&SensorID + SENSOR_ID_SIZE),(uint8_t*)&data);

  debug_println("Setting 485 ID");
  sprintf(buff, "$00SA%02X", SensorID);
  RS485_sendCommand((uint8_t*)buff, strlen(buff),NULL);

  debug_println("Sensor Id: %d",SensorID);
}

void sensorId(void)
{
  debug_println("Sensor Id: %d",SensorID);
}

EmberCommandEntry sensorSetCommands[] = {
  emberCommandEntryActionWithDetails("id", sensorSetId, "u", "", NULL),
  emberCommandEntryTerminator()
};

EmberCommandEntry sensorCommands[] = {
  emberCommandEntrySubMenu("set", sensorSetCommands, ""),
  emberCommandEntryActionWithDetails("id", sensorId, "", "", NULL),
  emberCommandEntryTerminator()
};


void rs485Init(void)
{

  RS485_Init();
  debug_println("RS485 succesfully configurated");
}

void rs485send(void)
{

  uint8_t len;
  uint8_t *data = emberStringCommandArgument(0,&len);
  debug_println("Sending %d bytes over rs485 port",len);
  RS485_sendCommand(data, len,rs485_DataReceived);

}

void rs485_DataReceived(RS485_Response_Buffer data)
{
  debug_println("Bytes received %d",data.ResponseSize);
  debug_println("response %s",data.ResponseData);
}


EmberCommandEntry rs845Commands[] = {
  emberCommandEntryActionWithDetails("init", rs485Init, "", "Initializes rs485 communication port ", NULL),
  emberCommandEntryActionWithDetails("send", rs485send, "b", "send data ", NULL),
  emberCommandEntryTerminator()
};

PGM_P PGM emberCommandTableEchoCommandArguments[] = {
  "bool indicating whether to turn echo on (1) or off (0)",
  NULL
};

void echoCommand(void)
{
  uint8_t echoOn = (uint8_t)emberUnsignedCommandArgument(0);
  if ( echoOn ) {
    emberCommandInterpreterEchoOn();
  } else {
    emberCommandInterpreterEchoOff();
  }
}



void wifiSend(void)
{
  uint8_t len;
  uint8_t *data = emberStringCommandArgument(0,&len);
  debug_println("Sending %d bytes via TCP/IP",len);
  wifiSendData(data,len);

}

void wifiSetSSID(void)
{
  uint8_t len;
  uint8_t *data = emberStringCommandArgument(0,&len);
  char SSID_temp[32];

  memset((void*)&SSID_temp,0,32);
  memcpy((void*)&SSID_temp,(void*)data,len);
  FLASH_write_entry((uint32_t)&SSID,((uint32_t)&SSID + SSID_SIZE),(uint8_t*)&SSID_temp);

  debug_println("SSID set to: %s",SSID);
}
void wifiSetPassword(void)
{
  uint8_t len;
  uint8_t *data = emberStringCommandArgument(0,&len);
  char Password_temp[64];

  memset((void*)&Password_temp,0,64);
  memcpy((void*)&Password_temp,(void*)data,len);
  FLASH_write_entry((uint32_t)&Password,((uint32_t)&Password + PASSWORD_SIZE),(uint8_t*)&Password_temp);

  debug_println("Password set to: %s",Password);
}
void wifiSetServerIP(void)
{
  uint8_t len;
  uint8_t *data = emberStringCommandArgument(0,&len);
  char ServerIP_temp[32];

  debug_println("len %d",len);
  debug_println("string %s",data);

  memset((void*)&ServerIP_temp,0,32);
  memcpy((void*)&ServerIP_temp,(void*)data,len);

  debug_println("string %s",ServerIP_temp);

  FLASH_write_entry((uint32_t)&ServerIP,(((uint32_t)&ServerIP + SERVER_IP_SIZE)),(uint8_t*)&ServerIP_temp);

  debug_println("Server IP set to: %s",ServerIP);
}
void wifiSetPort(void)
{
  uint32_t data = (uint32_t)emberUnsignedCommandArgument(0);

  FLASH_write_entry((uint32_t)&socketPort,((uint32_t)&socketPort + SOCKET_PORT_SIZE),(uint8_t*)&data);

  debug_println("Socket Port set to: %d",socketPort);
}



void wifiInfo(void)
{  
  debug_println("SSID: %s",SSID);
  debug_println("Password: %s",Password);
  debug_println("Server IP: %s",ServerIP);
  debug_println("Socket port: %d",socketPort);
}

void wifiClear(void)
{
  char data[64];
  memset((void*)&data,0,64);

  FLASH_write_entry((uint32_t)&SSID,(uint32_t)&SSID + sizeof(SSID),(uint8_t*)&data);
  FLASH_write_entry((uint32_t)&Password,(uint32_t)&Password + sizeof(Password),(uint8_t*)&data);
  FLASH_write_entry((uint32_t)&ServerIP,(uint32_t)&ServerIP + 32,(uint8_t*)&data);
  FLASH_write_entry((uint32_t)&socketPort,(uint32_t)&socketPort + sizeof(socketPort),(uint8_t*)&data);  
  
  debug_println("Wifi settings cleared");
}

void wifiSettings(void)
{
  uint8_t len;
  uint8_t *ssid;
  uint8_t *password;
  uint8_t *serverip;
  uint32_t port;

  char SSID_temp[32];
  char Password_temp[64];
  char ServerIP_temp[32];

  ssid = emberStringCommandArgument(0,&len);
  memset((void*)&SSID_temp,0,32);
  memcpy((void*)&SSID_temp,(void*)ssid,len);

  password = emberStringCommandArgument(1,&len);
  memset((void*)&Password_temp,0,64);
  memcpy((void*)&Password_temp,(void*)password,len);

  serverip = emberStringCommandArgument(2,&len);
  memset((void*)&ServerIP_temp,0,32);
  memcpy((void*)&ServerIP_temp,(void*)serverip,len);

  port = (uint32_t)emberUnsignedCommandArgument(3);

  FLASH_write_entry((uint32_t)&SSID,((uint32_t)&SSID + SSID_SIZE),(uint8_t*)&ssid);
  FLASH_write_entry((uint32_t)&Password,((uint32_t)&Password + PASSWORD_SIZE),(uint8_t*)&password);
  FLASH_write_entry((uint32_t)&ServerIP,(((uint32_t)&ServerIP + SERVER_IP_SIZE)),(uint8_t*)&serverip);
  FLASH_write_entry((uint32_t)&socketPort,((uint32_t)&socketPort + SOCKET_PORT_SIZE),(uint8_t*)&port);

  wifiInfo();
}

EmberCommandEntry wifiSetCommands[] = {
  emberCommandEntryActionWithDetails("ssid", wifiSetSSID, "b", "", NULL),
  emberCommandEntryActionWithDetails("password", wifiSetPassword, "b", "", NULL),
  emberCommandEntryActionWithDetails("serverip", wifiSetServerIP, "b", "", NULL),
  emberCommandEntryActionWithDetails("port", wifiSetPort, "w", "", NULL),
  emberCommandEntryActionWithDetails("clear", wifiClear, "", "", NULL),
  emberCommandEntryTerminator()
};


EmberCommandEntry wifiCommands[] = {
  emberCommandEntrySubMenu("set", wifiSetCommands, ""),
  emberCommandEntryActionWithDetails("settings", wifiSettings, "bbbw", "", NULL),
  emberCommandEntryActionWithDetails("info", wifiInfo, "", "Displays wifi info ", NULL),
  emberCommandEntryActionWithDetails("connect", wifiConnect, "", "Connects to WIFI", NULL),
  emberCommandEntryActionWithDetails("send", wifiSend, "b", "sends data via TCP/IP ", NULL),
  emberCommandEntryTerminator()
};

void reset(void)
{
  HAL_NVIC_SystemReset();
}
/*
EmberCommandEntry customTableCommands[] = {
  emberCommandEntryTerminator()
};
*/

EmberCommandEntry emberCommandTable[] = {
  emberCommandEntryActionWithDetails("events", printEvents, "", "Print the list of timer events.", NULL),
  emberCommandEntryAction("version", version,"", "Args: None"),
  emberCommandEntrySubMenu("sensor", sensorCommands, ""),
  emberCommandEntrySubMenu("rs485", rs845Commands, ""),
  emberCommandEntrySubMenu("wifi", wifiCommands, ""),
  emberCommandEntryActionWithDetails("echo", echoCommand, "u", "Turns echo on the command line on or off depending on the argument", emberCommandTableEchoCommandArguments),
	emberCommandEntryActionWithDetails("reset", reset, "", "Force to reset", emberCommandTableEchoCommandArguments),
  //emberCommandEntrySubMenu("custom", customTableCommands, ""),
  emberCommandEntryTerminator()
};
