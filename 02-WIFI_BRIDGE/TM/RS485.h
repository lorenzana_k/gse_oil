/**
 * @author  
 * @email   
 * @website 
 * @link    
 * @version 
 * @ide     
 * @license 
 * @brief   
 *  
\verbatim
   ----------------------------------------------------------------------
    Copyright (c) 2017

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without restriction,
    including without limitation the rights to use, copy, modify, merge,
    publish, distribute, sublicense, and/or sell copies of the Software, 
    and to permit persons to whom the Software is furnished to do so, 
    subject to the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
    AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
   ----------------------------------------------------------------------
\endverbatim
 */
#ifndef RS485_H
#define RS485_H

/* C++ detection */
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @addtogroup TM_STM32Fxxx_HAL_Libraries
 * @{
 */

/**
 * @defgroup MAX31865
 * @brief    
 * @{
 *
 */
#include "tm_stm32_spi.h"

/**
 * @defgroup MAX31865_Macros
 * @brief    Library defines
 * @{
 */

#define RS485_BUFFER_SIZE   30
    
/**
 * @brief MAX31865 Register addresses
 */

  

/*Read/Write bit*/


 /**
 * @}
 */
 
/**
 * @defgroup MAX31865_Typedefs
 * @brief    Library Typedefs
 * @{
 */

typedef struct
{
    uint8_t ResponseData[RS485_BUFFER_SIZE];
    uint8_t ResponseSize;
}RS485_Response_Buffer;

/**
 * @brief  I2C result enumeration
 */
typedef enum {
    RS485_Result_Ok = 0x00,             /*!< Everything OK */
    RS485_Result_Error,                 /*!< An error has occurred */
    RS485_Result_PortNotInitialized,    /*!< An error has occurred */
    RS485_Result_ReadyToSend,           /*!< An error has occurred */
    RS485_Result_WaitingResponse        /*!< An error has occurred */
} RS485_Result_t;




/**
 * @}
 */

/**
 * @defgroup 
 * @brief    Library Functions
 * @{
 */


RS485_Result_t RS485_Init(void);
RS485_Result_t RS485_sendCommand(uint8_t* InputData, uint8_t InputDataSize,void (*CB)(RS485_Response_Buffer response));
RS485_Result_t RS584_Status(void);

/**
 * @}
 */
 
/**
 * @}
 */
 
/**
 * @}
 */

/* C++ detection */
#ifdef __cplusplus
}
#endif

#endif

