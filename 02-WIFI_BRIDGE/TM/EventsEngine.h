//-----------------------------------------------------------------------------
//
//
//-----------------------------------------------------------------------------
#ifndef	__EVENTS_ENGINE_H__
#define	__EVENTS_ENGINE_H__

#include "stdint.h"


typedef uint8_t EventUnits;
enum
{
  /** The event is not scheduled to run. */
  EVENT_INACTIVE = 0,
  /** The execution time is in approximate milliseconds.  */
  EVENT_MS_TIME,
  /** The execution time is in 'binary' quarter seconds (256 approximate
      milliseconds each). */
  EVENT_QS_TIME,
  /** The execution time is in 'binary' minutes (65536 approximate milliseconds
      each). */
  EVENT_MINUTE_TIME,
  /** The event is scheduled to run at the earliest opportunity. */
  EVENT_ZERO_DELAY
};


/** @brief Control structure for events.
 *
 * This structure should not be accessed directly.
 * This holds the event status (one of the @e EVENT_ values)
 * and the time left before the event fires.
 */
typedef struct {
  /** The event's status, either inactive or the units for timeToExecute. */
  EventUnits status;

  /** How long before the event fires.
   *  Units are always in milliseconds
   */
  uint32_t timeToExecute;
} EventControl;

/** @brief Complete events with a control and a handler procedure.
 *
 * An application typically creates an array of events
 * along with their handlers.
 * The main loop passes the array to ::RunEvents() in order to call
 * the handlers of any events whose time has arrived.
 */
typedef const struct EventData_S {
  /** The control structure for the event. */
  EventControl *control;
  /** The procedure to call when the event fires. */
  void (*handler)(void);
} EventData;

/** @brief Sets this ::EmberEventControl as inactive (no pending event).
 */
#define EventControlSetInactive(control)    \
  do { (control).status = EVENT_INACTIVE; } while(0)

/** @brief Returns true if the event is active, false otherwise.
 */
#define EventControlGetActive(control)      \
  do { ((control).status != EVENT_INACTIVE); } while(0)

/** @brief Sets this ::EmberEventControl to run at the next available
    opportunity.
 */
#define EventControlSetActive(control)               \
  do { (control).status = EVENT_ZERO_DELAY; } while(0)

/** @brief Sets this ::EmberEventControl to run "delay" milliseconds in the future.
 *  NOTE: To avoid rollover errors in event calculation, the delay must be
 *  less than ::EMBER_MAX_EVENT_CONTROL_DELAY_MS.
 */
#define EventControlSetDelayMS(control, delay)        \
  do { (control).status = EVENT_MS_TIME; (control).timeToExecute = delay; } while(0)
  
/**
 * @brief a function used to run the application framework's
 *        event mechanism.
 */
void RunEvents(void);

/**
 * @brief Enables based time events
 */
void EventIT(void);
  
/**
 * @brief Initializes events used for the application.
 */
void EventInit(void);
  
/**
 * @brief Print the list of timer events
 */
void printEvents(void);

  
#endif
