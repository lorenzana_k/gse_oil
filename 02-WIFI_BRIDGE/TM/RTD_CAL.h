/**
 * @author  
 * @email   
 * @website 
 * @link    
 * @version 
 * @ide     
 * @license 
 * @brief   
 *  
\verbatim
   ----------------------------------------------------------------------
    Copyright (c) 2017

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without restriction,
    including without limitation the rights to use, copy, modify, merge,
    publish, distribute, sublicense, and/or sell copies of the Software, 
    and to permit persons to whom the Software is furnished to do so, 
    subject to the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
    AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
   ----------------------------------------------------------------------
\endverbatim
 */
#ifndef RTD_CAL__H
#define MAX31865_H

/* C++ detection */
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @addtogroup TM_STM32Fxxx_HAL_Libraries
 * @{
 */
#include <math.h>
/**
 * @defgroup 
 * @brief    
 * @{
 *
 */


/**
 * @defgroup MAX31865_Macros
 * @brief    Library defines
 * @{
 */

/* constants for PT1000 */
#define R0       (float)1000.0
#define CONST_A  (float)0.0039083
#define CONST_B  (float)-0.0000005775
#define CONST_1  (float)-3.9083
#define CONST_2  (float)15.27480889
#define CONST_3  (float)-0.001155
#define CONST_4  (float)0.00231
 /**
 * @}
 */
 
/**
 * @defgroup MAX31865_Typedefs
 * @brief    Library Typedefs
 * @{
 */



/**
 * @}
 */

/**
 * @defgroup 
 * @brief    Library Functions
 * @{
 */

/**
 * @brief      Calculates temperature from given resistance on a PT1000 sensor.
 *             https://techoverflow.net/2016/01/02/accurate-calculation-of-pt100pt1000-temperature-from-resistance/
 */
static inline float RTD_CAL_getTemperature(float Rt)
{
  float temperature;

  temperature = (CONST_1 + (sqrt(CONST_2 + (CONST_4*(R0-Rt)))) ) / CONST_3;
  return temperature;
}

/**
 * @brief   Calculates resistance on a PT1000 for a given temperature value.
 */
static inline float RTD_CAL_getResistance(float temp)
{
  float Resistance;

  Resistance = R0 * (1.0 + (CONST_A * temp) + (CONST_B * pow(temp,2) ) );
  return Resistance;
}



/**
 * @}
 */
 
/**
 * @}
 */
 
/**
 * @}
 */

/* C++ detection */
#ifdef __cplusplus
}
#endif

#endif

